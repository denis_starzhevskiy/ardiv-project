export enum LOCALES {
  ENGLISH = "en-Ug",
  RUSSIAN = "ru-RU",
  HEBREW = "he-IZ",

  FRANCE = "fr-FR",
}
