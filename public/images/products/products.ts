// import product1 from "@/public/images/products/product1.jpg";
// import product2 from "@/public/images/products/product2.jpg";
// import product3 from "@/public/images/products/product3.jpg";
// import product4 from "@/public/images/products/product4.jpg";
// import product6 from "@/public/images/products/product6.jpg";
// import product7 from "@/public/images/products/product7.jpg";
// import product8 from "@/public/images/products/product8.jpg";
// import product9 from "@/public/images/products/product9.jpg";
// import product10 from "@/public/images/products/product10.jpg";
// import product11 from "@/public/images/products/product11.jpg";
// import product12 from "@/public/images/products/product12.jpg";
// import product20 from "@/public/images/products/product20.jpg";
// import product13 from "@/public/images/products/product13.jpg";
// import product15 from "@/public/images/products/product15.jpg";
// import product16 from "@/public/images/products/product16.jpg";
// import product17 from "@/public/images/products/product17.jpg";

import product1 from "@/public/images/brands/white.jpg";
import product2 from "@/public/images/brands/white.jpg";
import product3 from "@/public/images/brands/white.jpg";
import product4 from "@/public/images/brands/white.jpg";
import product6 from "@/public/images/brands/white.jpg";
import product7 from "@/public/images/brands/white.jpg";
import product8 from "@/public/images/brands/white.jpg";
import product9 from "@/public/images/brands/white.jpg";
import product10 from "@/public/images/brands/white.jpg";
import product11 from "@/public/images/brands/white.jpg";
import product12 from "@/public/images/brands/white.jpg";
import product20 from "@/public/images/brands/white.jpg";
import product13 from "@/public/images/brands/white.jpg";
import product15 from "@/public/images/brands/white.jpg";
import product16 from "@/public/images/brands/white.jpg";
import product17 from "@/public/images/brands/white.jpg";

// export const productsIImages = [
//   {
//     image: product1,
//     label: "Оболонь",
//     link: "https://obolon.ua/",
//   },
//   {
//     image: product2,
//     label: "Бисквит-шоколад",
//     link: "https://biscuit.com.ua/",
//   },
//   {
//     image: product3,
//     label: "Махеев",
//     link: "http://www.maheev.ru/",
//   },
//   {
//     image: product4,
//     label: "Мороша",
//     link: "https://morosha.com/en",
//   },
//   {
//     image: product6,
//     label: "Макфа",
//     link: "http://www.makfa.ru/",
//   },
//   {
//     image: product7,
//     label: "Львивське",
//     link: "https://olymp.ua/",
//   },
//   {
//     image: product8,
//     label: "Хортица",
//     link: "https://en.khortytsa.com/",
//   },
//   {
//     image: product9,
//     label: "Pogues",
//     link: "https://thepoguesirishwhiskey.com/",
//   },
//   {
//     image: product10,
//     label: "Мивина",
//     link: "http://mivina.com/",
//   },
//   {
//     image: product11,
//     label: "Заречье",
//     link: "http://kmk.ua/ru/catalog/zareche",
//   },
//   {
//     image: product20,
//     label: "Шустов",
//     link: "http://www.tavria.ua/#&panel1-1",
//   },
//   {
//     image: product12,
//     label: "Crabbie",
//     link: "https://crabbiewhisky.com/",
//   },
//   {
//     image: product13,
//     label: "Флинт",
//     link: "http://snackproduction.com.ua/tm-flint/",
//   },
//   {
//     image: product16,
//     label: "Artwinery",
//     link: "https://artwinery.com.ua/ru/main.html",
//   },
//   {
//     image: product17,
//     label: "City of London Distillery",
//     link: "https://www.cityoflondondistillery.com/",
//   },
//   {
//     image: product15,
//     label: "West Cork",
//     link: "https://www.westcorkdistillers.com/",
//   },
// ];

export const productsIImages = [
  {
    image: product1,
    label: "Заголовок",
    link: "https://obolon.ua/",
  },
  {
    image: product2,
    label: "Заголовок",
    link: "https://biscuit.com.ua/",
  },
  {
    image: product3,
    label: "Заголовок",
    link: "http://www.maheev.ru/",
  },
  {
    image: product4,
    label: "Заголовок",
    link: "https://morosha.com/en",
  },
  {
    image: product6,
    label: "Заголовок",
    link: "http://www.makfa.ru/",
  },
  {
    image: product7,
    label: "Заголовок",
    link: "https://olymp.ua/",
  },
  {
    image: product8,
    label: "Заголовок",
    link: "https://en.khortytsa.com/",
  },
  {
    image: product9,
    label: "Заголовок",
    link: "https://thepoguesirishwhiskey.com/",
  },
  {
    image: product10,
    label: "Заголовок",
    link: "http://mivina.com/",
  },
  {
    image: product11,
    label: "Заголовок",
    link: "http://kmk.ua/ru/catalog/zareche",
  },
  {
    image: product20,
    label: "Заголовок",
    link: "http://www.tavria.ua/#&panel1-1",
  },
  {
    image: product12,
    label: "Заголовок",
    link: "https://crabbiewhisky.com/",
  },
  {
    image: product13,
    label: "Заголовок",
    link: "http://snackproduction.com.ua/tm-flint/",
  },
  {
    image: product16,
    label: "Заголовок",
    link: "https://artwinery.com.ua/ru/main.html",
  },
  {
    image: product17,
    label: "Заголовок",
    link: "https://www.cityoflondondistillery.com/",
  },
  {
    image: product15,
    label: "Заголовок",
    link: "https://www.westcorkdistillers.com/",
  },
];
