import popular1 from "@/public/images/brandProducts/DONKO/4111.jpg";
import popular2 from "@/public/images/brandProducts/DONKO/4112.jpg";
import popular3 from "@/public/images/brandProducts/DONKO/4113.jpg";
import popular4 from "@/public/images/brandProducts/DONKO/4221.jpg";
import popular5 from "@/public/images/brandProducts/DONKO/4222.jpg";
import popular6 from "@/public/images/brandProducts/DONKO/4223.jpg";
import popular7 from "@/public/images/brandProducts/DONKO/4224.jpg";
import popular8 from "@/public/images/brandProducts/DONKO/4331.jpg";
import popular9 from "@/public/images/brandProducts/DONKO/4332.jpg";
import popular10 from "@/public/images/brandProducts/DONKO/4333.jpg";
import popular11 from "@/public/images/brandProducts/DONKO/4334.jpg";
import popular12 from "@/public/images/brandProducts/DONKO/4551.jpg";
import popular13 from "@/public/images/brandProducts/DONKO/4552.jpg";
import popular14 from "@/public/images/brandProducts/DONKO/4553.jpg";
import popular15 from "@/public/images/brandProducts/DONKO/4554.jpg";
import popular16 from "@/public/images/brandProducts/DONKO/4555.jpg";
import popular17 from "@/public/images/brandProducts/DONKO/4556.jpg";
import popular18 from "@/public/images/brandProducts/DONKO/4557.jpg";
import popular19 from "@/public/images/brandProducts/DONKO/4558.jpg";
import popular20 from "@/public/images/brandProducts/DONKO/4661.jpg";
import popular21 from "@/public/images/brandProducts/DONKO/4662.jpg";
import popular22 from "@/public/images/brandProducts/DONKO/4663.jpg";
import popular23 from "@/public/images/brandProducts/DONKO/4610122140773.jpg";
import popular24 from "@/public/images/brandProducts/DONKO/4610122140810.jpg";
import popular25 from "@/public/images/brandProducts/DONKO/4610122147871.jpg";
import popular26 from "@/public/images/brandProducts/DONKO/4610122147895.jpg";
import popular27 from "@/public/images/brandProducts/DONKO/4650093005916.jpg";
import popular28 from "@/public/images/brandProducts/DONKO/4650093001468.jpg";
import popular29 from "@/public/images/brandProducts/DONKO/4650093000249.jpg";
import popular30 from "@/public/images/brandProducts/DONKO/4441.jpg";
import popular31 from "@/public/images/brandProducts/DONKO/4442.jpg";
import popular32 from "@/public/images/brandProducts/DONKO/4443.jpg";
import popular33 from "@/public/images/brandProducts/DONKO/4444.jpg";

export const DONKO = "Donko ";

export const donkoImages = {
  "Wafer cakes": [
    {
      image: popular1,
    },
    {
      image: popular2,
    },
    {
      image: popular3,
    },
  ],
  "Senator ": [
    {
      image: popular4,
    },
    {
      image: popular5,
    },
    {
      image: popular6,
    },
    {
      image: popular7,
    },
  ],
  "Rolls mini": [
    {
      image: popular8,
    },
    {
      image: popular9,
    },
    {
      image: popular10,
    },
    {
      image: popular11,
    },
  ],
  "Rolls ": [
    {
      image: popular30,
    },
    {
      image: popular31,
    },
    {
      image: popular32,
    },
    {
      image: popular33,
    },
  ],
  "Esfero ": [
    {
      image: popular12,
    },
    {
      image: popular13,
    },
    {
      image: popular14,
    },
    {
      image: popular15,
    },
    {
      image: popular16,
    },
    {
      image: popular17,
    },
    {
      image: popular18,
    },
    {
      image: popular19,
    },
  ],
  "Dolci ": [
    {
      image: popular23,
    },
    {
      image: popular24,
    },
    {
      image: popular25,
    },
    {
      image: popular26,
    },
    {
      image: popular27,
    },
    {
      image: popular28,
    },
    {
      image: popular29,
    },
  ],
  "Cookie ": [
    {
      image: popular20,
    },
    {
      image: popular21,
    },
    {
      image: popular22,
    },
  ],
};
