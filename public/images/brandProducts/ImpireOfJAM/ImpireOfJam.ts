import popular1 from "@/public/images/brandProducts/ImpireOfJAM/4607167191457.jpg";
import popular2 from "@/public/images/brandProducts/ImpireOfJAM/4607167195134.jpg";
import popular3 from "@/public/images/brandProducts/ImpireOfJAM/4607167195066.jpg";
import popular4 from "@/public/images/brandProducts/ImpireOfJAM/4607167195158.jpg";
import popular5 from "@/public/images/brandProducts/ImpireOfJAM/4607167195196.jpg";
import popular6 from "@/public/images/brandProducts/ImpireOfJAM/4607167195172.jpg";
import popular7 from "@/public/images/brandProducts/ImpireOfJAM/4607167195202.jpg";
import popular8 from "@/public/images/brandProducts/ImpireOfJAM/4607167195226.jpg";
import popular9 from "@/public/images/brandProducts/ImpireOfJAM/4607167195233.jpg";
import popular10 from "@/public/images/brandProducts/ImpireOfJAM/4607167195257.jpg";
import popular11 from "@/public/images/brandProducts/ImpireOfJAM/4607167195318.jpg";


export const IMPIREOFJAM = 'ImpireOfJam'


export const impireOfJamImages = [
    {
        image: popular1,
    },
    {
        image: popular2,
    },
    {
        image: popular3,
    },
    {
        image: popular4,
    },
    {
        image: popular5,
    },
    {
        image: popular6,
    },
    {
        image: popular7,
    },
    {
        image: popular8,
    },
    {
        image: popular9,
    },
    {
        image: popular10,
    },
    {
        image: popular11,
    },


];
