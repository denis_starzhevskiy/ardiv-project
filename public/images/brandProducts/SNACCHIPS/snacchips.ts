import popular1 from "@/public/images/brandProducts/SNACCHIPS/8856307836109.jpg";
import popular2 from "@/public/images/brandProducts/SNACCHIPS/8856307836116.jpg";
import popular3 from "@/public/images/brandProducts/SNACCHIPS/8856307836215.jpg";
import popular4 from "@/public/images/brandProducts/SNACCHIPS/8856307836314.jpg";
import popular5 from "@/public/images/brandProducts/SNACCHIPS/8856307836505.jpg";
import popular6 from "@/public/images/brandProducts/SNACCHIPS/8856307836406.jpg";

export const SNACCHIPS = 'Snacchips'

export const snacchipsImages = [
    {
        image: popular1,
    },
    {
        image: popular2,

    },
    {
        image: popular3,

    },
    {
        image: popular4,
    },
    {
        image: popular5,
    },
    {
        image: popular6,
    },

];
