import popular1 from "@/public/images/brandProducts/DELICATE/4810389004089.jpg";
import popular2 from "@/public/images/brandProducts/DELICATE/4810389004065.jpg";
import popular3 from "@/public/images/brandProducts/DELICATE/4810389004386.jpg";
import popular4 from "@/public/images/brandProducts/DELICATE/4810389006854.jpg";
import popular5 from "@/public/images/brandProducts/DELICATE/4810389006434.jpg";
import popular6 from "@/public/images/brandProducts/DELICATE/4810389006441.jpg";
import popular7 from "@/public/images/brandProducts/DELICATE/4810389006502.jpg";
import popular8 from "@/public/images/brandProducts/DELICATE/4810389006427.jpg";
import popular9 from "@/public/images/brandProducts/DELICATE/4810389005284.jpg";
import popular10 from "@/public/images/brandProducts/DELICATE/4810389005291.jpg";
import popular11 from "@/public/images/brandProducts/DELICATE/4810389006359.jpg";
import popular12 from "@/public/images/brandProducts/DELICATE/4/4810389006540.jpg";
import popular13 from "@/public/images/brandProducts/DELICATE/4/4810389006557.jpg";
import popular14 from "@/public/images/brandProducts/DELICATE/4/4810389007592.jpg";
import popular15 from "@/public/images/brandProducts/DELICATE/4/4810389006564.jpg";
import popular16 from "@/public/images/brandProducts/DELICATE/4/4810389006533.jpg";
import popular17 from "@/public/images/brandProducts/DELICATE/4/4810389007615.jpg";
import popular18 from "@/public/images/brandProducts/DELICATE/4/4810389007790.jpg";
import popular19 from "@/public/images/brandProducts/DELICATE/4/4810389007608.jpg";

export const DELICATE = "Delicate";

export const delicateNewImages = {
  "Майонезы ": [
    {
      image: popular9,
    },
    {
      image: popular10,
    },
    {
      image: popular11,
    },
  ],
  "Соусы ": [
    {
      image: popular12,
    },
    {
      image: popular13,
    },
    {
      image: popular14,
    },
    {
      image: popular15,
    },
    {
      image: popular16,
    },
    {
      image: popular17,
    },
    {
      image: popular18,
    },
    {
      image: popular19,
    },
  ],
  "Кетчупы ": [
    {
      image: popular5,
    },
    {
      image: popular6,
    },
    {
      image: popular7,
    },
    {
      image: popular8,
    },
  ],
  "Горчица/Хрен": [
    {
      image: popular1,
    },
    {
      image: popular2,
    },
    {
      image: popular3,
    },
    {
      image: popular4,
    },
  ],
};
