import popular1 from "@/public/images/brandProducts/Voronzovsky/4607806761591.jpg";
import popular2 from "@/public/images/brandProducts/Voronzovsky/4620011190048.jpg";
import popular3 from "@/public/images/brandProducts/Voronzovsky/4607806761423.jpg";
import popular4 from "@/public/images/brandProducts/Voronzovsky/4620011190123.jpg";
import popular5 from "@/public/images/brandProducts/Voronzovsky/4620011194220.jpg";
import popular6 from "@/public/images/brandProducts/Voronzovsky/4620011190598.jpg";
import popular7 from "@/public/images/brandProducts/Voronzovsky/4620011190697.jpg";
import popular8 from "@/public/images/brandProducts/Voronzovsky/4620011190253.jpg";

export const VORONZOVSKY = "Воронцовские";

export const voronzovskyImages = {
  "Гренки ": [
    {
      image: popular1,
    },
    {
      image: popular5,
    },
    {
      image: popular6,
    },
    {
      image: popular7,
    },
  ],
  "Сухарики ": [
    {
      image: popular2,
    },
    {
      image: popular3,
    },
    {
      image: popular4,
    },
    {
      image: popular8,
    },
  ],
};
