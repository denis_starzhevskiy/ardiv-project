import popular1 from "@/public/images/brandProducts/CannedMILK/4820012680849 GOST.jpg";
import popular2 from "@/public/images/brandProducts/CannedMILK/4820012683536.jpg";
import popular3 from "@/public/images/brandProducts/CannedMILK/4820012681372 copy.jpg";
import popular4 from "@/public/images/brandProducts/CannedMILK/4820012683550 cacao copy.jpg";
import popular5 from "@/public/images/brandProducts/CannedMILK/48500.jpg";
import popular6 from "@/public/images/brandProducts/CannedMILK/485002.jpg";
import popular7 from "@/public/images/brandProducts/CannedMILK/485001.jpg";
import popular8 from "@/public/images/brandProducts/CannedMILK/485003.jpg";
import popular9 from "@/public/images/brandProducts/CannedMILK/485004.jpg";

export const CANNEDMILK = "CannedMilk";

export const cannedMilkNewImages = {
  Сырки: [
    {
      image: popular5,
    },
    {
      image: popular6,
    },
    {
      image: popular7,
    },
    {
      image: popular8,
    },
    {
      image: popular9,
    },
  ],
  Сгущенка: [
    {
      image: popular1,
    },
    {
      image: popular2,
    },
    {
      image: popular3,
    },
    {
      image: popular4,
    },
  ],
};
