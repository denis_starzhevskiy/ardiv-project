import popular1 from "@/public/images/brandProducts/AGEEVSKY/4620006295758.jpg";
import popular2 from "@/public/images/brandProducts/AGEEVSKY/4620006295802.jpg";
import popular3 from "@/public/images/brandProducts/AGEEVSKY/4620006295765.jpg";
import popular4 from "@/public/images/brandProducts/AGEEVSKY/4620006295963.jpg";
import popular5 from "@/public/images/brandProducts/AGEEVSKY/4620006295994.jpg";
import popular6 from "@/public/images/brandProducts/AGEEVSKY/4620006295987.jpg";
import popular7 from "@/public/images/brandProducts/AGEEVSKY/4620006296038.jpg";
import popular8 from "@/public/images/brandProducts/AGEEVSKY/4620006296069.jpg";
import popular9 from "@/public/images/brandProducts/AGEEVSKY/4620006296076.jpg";
import popular10 from "@/public/images/brandProducts/AGEEVSKY/4620006296090.jpg";
import popular11 from "@/public/images/brandProducts/AGEEVSKY/4620006296113.jpg";
import popular12 from "@/public/images/brandProducts/AGEEVSKY/4620006296120.jpg";
import popular13 from "@/public/images/brandProducts/AGEEVSKY/4620006296144.jpg";
import popular14 from "@/public/images/brandProducts/AGEEVSKY/4620006296151.jpg";
import popular15 from "@/public/images/brandProducts/AGEEVSKY/4620006296168.jpg";
import popular16 from "@/public/images/brandProducts/AGEEVSKY/4620006296182.jpg";
import popular17 from "@/public/images/brandProducts/AGEEVSKY/4620006296205.jpg";
import popular18 from "@/public/images/brandProducts/AGEEVSKY/4620006296212.jpg";
import popular19 from "@/public/images/brandProducts/AGEEVSKY/4620006296236.jpg";
import popular20 from "@/public/images/brandProducts/AGEEVSKY/4620006296243.jpg";
import popular21 from "@/public/images/brandProducts/AGEEVSKY/4620006296250.jpg";
import popular22 from "@/public/images/brandProducts/AGEEVSKY/4620006296267.jpg";
import popular23 from "@/public/images/brandProducts/AGEEVSKY/4620006296281.jpg";
import popular24 from "@/public/images/brandProducts/AGEEVSKY/4620006296298.jpg";
import popular25 from "@/public/images/brandProducts/AGEEVSKY/4650001081711.jpg";
import popular26 from "@/public/images/brandProducts/AGEEVSKY/4650001081650.jpg";
import popular27 from "@/public/images/brandProducts/AGEEVSKY/4650001081643.jpg";
import popular28 from "@/public/images/brandProducts/AGEEVSKY/4650001081636.jpg";
import popular29 from "@/public/images/brandProducts/AGEEVSKY/4650001081629.jpg";

export const AGEEVSKY = 'Ageevsky'

export const ageevskymages  = [
    {
        image: popular25,
    },
    {
        image: popular26,
    },
    {
        image: popular27,
    },
    {
        image: popular28,
    },
    {
        image: popular29,
    },
    {
        image: popular1,
    },
    {
        image: popular2,

    },
    {
        image: popular3,

    },
    {
        image: popular4,

    },
    {
        image: popular5,

    },
    {
        image: popular6,

    },
    {
        image: popular7,

    },
    {
        image: popular8,

    },
    {
        image: popular9,

    },
    {
        image: popular10,

    },
    {
        image: popular11,

    },
    {
        image: popular12,

    },
    {
        image: popular13,

    },
    {
        image: popular14,

    },
    {
        image: popular15,

    },
    {
        image: popular16,

    },
    {
        image: popular17,

    },
    {
        image: popular18,

    },
    {
        image: popular19,

    },
    {
        image: popular20,

    },
    {
        image: popular21,

    },
    {
        image: popular22,

    },
    {
        image: popular23,

    },
    {
        image: popular24,

    },


];