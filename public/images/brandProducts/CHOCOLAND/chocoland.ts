import popular1 from "@/public/images/brandProducts/CHOCOLAND/4640132830036.jpg";
import popular2 from "@/public/images/brandProducts/CHOCOLAND/4640132830210.jpg";
import popular3 from "@/public/images/brandProducts/CHOCOLAND/4640132830197.jpg";
import popular4 from "@/public/images/brandProducts/CHOCOLAND/4640132830357.jpg";
import popular5 from "@/public/images/brandProducts/CHOCOLAND/4640132830371.jpg";
import popular6 from "@/public/images/brandProducts/CHOCOLAND/4640132830470.jpg";
import popular7 from "@/public/images/brandProducts/CHOCOLAND/4640132830494.jpg";
import popular8 from "@/public/images/brandProducts/CHOCOLAND/4640132830517.jpg";
import popular9 from "@/public/images/brandProducts/CHOCOLAND/4640132830531.jpg";
import popular10 from "@/public/images/brandProducts/CHOCOLAND/4640132830555.jpg";
import popular11 from "@/public/images/brandProducts/CHOCOLAND/4640132830814.jpg";
import popular12 from "@/public/images/brandProducts/CHOCOLAND/4640132830869.jpg";
import popular13 from "@/public/images/brandProducts/CHOCOLAND/4640132831293.jpg";
import popular14 from "@/public/images/brandProducts/CHOCOLAND/4640132831316.jpg";
import popular15 from "@/public/images/brandProducts/CHOCOLAND/4640132831750.jpg";
import popular16 from "@/public/images/brandProducts/CHOCOLAND/4640132831804.jpg";
import popular17 from "@/public/images/brandProducts/CHOCOLAND/4640132832054.jpg";


export const CHOCOLAND = 'Chocoland'


export const chocolandImages = [
    {
        image: popular1,
    },
    {
        image: popular2,

    },
    {
        image: popular3,

    },
    {
        image: popular4,
    },
    {
        image: popular5,
    },
    {
        image: popular6,
    },
    {
        image: popular7,
    },
    {
        image: popular8,
    },
    {
        image: popular9,
    },
    {
        image: popular10,
    },
    {
        image: popular11,
    },
    {
        image: popular12,
    },
    {
        image: popular13,
    },
    {
        image: popular14,
    },
    {
        image: popular15,
    },
    {
        image: popular16,
    },
    {
        image: popular17,
    },

];