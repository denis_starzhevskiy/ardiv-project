import popular1 from "@/public/images/brandProducts/Muchskino/4610174790070.jpg";
import popular2 from "@/public/images/brandProducts/Muchskino/4610174790285.jpg";
import popular3 from "@/public/images/brandProducts/Muchskino/4610174790247.jpg";
import popular4 from "@/public/images/brandProducts/Muchskino/4610174790438.jpg";
import popular5 from "@/public/images/brandProducts/Muchskino/4610174790452.jpg";
import popular6 from "@/public/images/brandProducts/Muchskino/4610174790445.jpg";
import popular7 from "@/public/images/brandProducts/Muchskino/4610174790681.jpg";
import popular8 from "@/public/images/brandProducts/Muchskino/4610174790704.jpg";
import popular9 from "@/public/images/brandProducts/Muchskino/4610174790704.jpg";
import popular10 from "@/public/images/brandProducts/Muchskino/4610174790711.jpg";
import popular11 from "@/public/images/brandProducts/Muchskino/4610174790889.jpg";
import popular12 from "@/public/images/brandProducts/Muchskino/4610174790896.jpg";
import popular13 from "@/public/images/brandProducts/Muchskino/4610174790957.jpg";
import popular14 from "@/public/images/brandProducts/Muchskino/4610174790964.jpg";
import popular15 from "@/public/images/brandProducts/Muchskino/4610174791060.jpg";
import popular16 from "@/public/images/brandProducts/Muchskino/4610174791138.jpg";
import popular17 from "@/public/images/brandProducts/Muchskino/4610174791497.jpg";
import popular18 from "@/public/images/brandProducts/Muchskino/4610174793668.jpg";
import popular19 from "@/public/images/brandProducts/Muchskino/4610174793729.jpg";
import popular20 from "@/public/images/brandProducts/Muchskino/4610174793743.jpg";
import popular21 from "@/public/images/brandProducts/Muchskino/4610174793811.jpg";
import popular22 from "@/public/images/brandProducts/Muchskino/4610174794245.jpg";
import popular23 from "@/public/images/brandProducts/Muchskino/4610174794238.jpg";
import popular24 from "@/public/images/brandProducts/Muchskino/4610174793835.jpg";

export const MUCHSKINO = 'Muchskino'

export const muchskinoImages = [
    {
        image: popular1,
    },
    {
        image: popular2,

    },
    {
        image: popular3,

    },
    {
        image: popular4,
    },
    {
        image: popular5,
    },
    {
        image: popular6,
    },
    {
        image: popular7,
    },
    {
        image: popular8,
    },
    {
        image: popular9,
    },
    {
        image: popular10,
    },
    {
        image: popular11,
    },
    {
        image: popular12,
    },
    {
        image: popular13,
    },
    {
        image: popular14,
    },
    {
        image: popular15,
    },
    {
        image: popular16,
    },
    {
        image: popular17,
    },
    {
        image: popular18,
    },
    {
        image: popular19,
    },
    {
        image: popular20,
    },
    {
        image: popular21,
    },
    {
        image: popular22,
    },
    {
        image: popular23,
    },
    {
        image: popular24,
    },

];

