import popular1 from "@/public/images/brandProducts/SINGOFCARE/4607039947779.jpg";
import popular2 from "@/public/images/brandProducts/SINGOFCARE/4607039946079.jpg";
import popular3 from "@/public/images/brandProducts/SINGOFCARE/4607039946031.jpg";
import popular4 from "@/public/images/brandProducts/SINGOFCARE/4607039946024.jpg";
import popular5 from "@/public/images/brandProducts/SINGOFCARE/4607039947786.jpg";
import popular6 from "@/public/images/brandProducts/SINGOFCARE/4607039946598.jpg";
import popular7 from "@/public/images/brandProducts/SINGOFCARE/4607039946000.jpg";
import popular8 from "@/public/images/brandProducts/SINGOFCARE/4607039946604.jpg";
import popular9 from "@/public/images/brandProducts/SINGOFCARE/4607039946642.jpg";
import popular10 from "@/public/images/brandProducts/SINGOFCARE/4607039947878.jpg";
import popular11 from "@/public/images/brandProducts/SINGOFCARE/4607039947885.jpg";


export const SINGOFCARE = 'Sing of Care';

export const singOfCareNewImages = {
    'Кукуруза': [
        {
            image: popular5,
        },

        {
            image: popular8,
        },

        {
            image: popular10,
        },
    ],
    'Горошек':[
        {
            image: popular1,
        },
        {
            image: popular2,

        },
        {
            image: popular3,

        },
        {
            image: popular4,
        },
        {
            image: popular6,
        },
        {
            image: popular7,
        },
        {
            image: popular9,
        },
        {
            image: popular11,
        },
    ]
}





