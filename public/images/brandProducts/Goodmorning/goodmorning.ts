import popular1 from "@/public/images/brandProducts/Goodmorning/4607051546899.jpg";
import popular2 from "@/public/images/brandProducts/Goodmorning/4607051545915.jpg";
import popular3 from "@/public/images/brandProducts/Goodmorning/4607051545922.jpg";
import popular4 from "@/public/images/brandProducts/Goodmorning/4607051545908.jpg";
import popular5 from "@/public/images/brandProducts/Goodmorning/4607051541283.jpg";
import popular6 from "@/public/images/brandProducts/Goodmorning/4607051546868.jpg";
import popular7 from "@/public/images/brandProducts/Goodmorning/4607051541672.jpg";

export const GOODMORNING = 'Goodmorning'

export const goodmorningImages = [
    {
        image: popular1,
    },
    {
        image: popular2,

    },
    {
        image: popular3,

    },
    {
        image: popular4,
    },
    {
        image: popular5,
    },
    {
        image: popular6,
    },
    {
        image: popular7,
    },

];
