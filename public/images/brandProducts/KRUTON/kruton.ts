import popular1 from "@/public/images/brandProducts/KRUTON/4820217950709.jpg";
import popular2 from "@/public/images/brandProducts/KRUTON/4820217950761.jpg";
import popular3 from "@/public/images/brandProducts/KRUTON/4820217950723.jpg";
import popular4 from "@/public/images/brandProducts/KRUTON/4820217950778.jpg";
import popular5 from "@/public/images/brandProducts/KRUTON/4820217950785.jpg";
import popular6 from "@/public/images/brandProducts/KRUTON/4820217950815.jpg";
import popular7 from "@/public/images/brandProducts/KRUTON/4820217950822.jpg";


export const KRUTON = 'Kruton'

export const krutonImages = [
    {
        image: popular1,
    },
    {
        image: popular2,

    },
    {
        image: popular3,

    },
    {
        image: popular4,
    },
    {
        image: popular5,
    },
    {
        image: popular6,
    },
    {
        image: popular7,
    },

];
