import popular1 from "@/public/images/brandProducts/Wishbrand/4660147900340.jpg";
import popular2 from "@/public/images/brandProducts/Wishbrand/4660147900395.jpg";
import popular3 from "@/public/images/brandProducts/Wishbrand/4660147900371.jpg";
import popular4 from "@/public/images/brandProducts/Wishbrand/4660147900432.jpg";
import popular5 from "@/public/images/brandProducts/Wishbrand/4660147900753.jpg";
import popular6 from "@/public/images/brandProducts/Wishbrand/4660147900463.jpg";
import popular7 from "@/public/images/brandProducts/Wishbrand/4660147900814.jpg";
import popular8 from "@/public/images/brandProducts/Wishbrand/4660147900951.jpg";
import popular9 from "@/public/images/brandProducts/Wishbrand/4660147901033.jpg";
import popular10 from "@/public/images/brandProducts/Wishbrand/4660147901071.jpg";
import popular11 from "@/public/images/brandProducts/Wishbrand/4660147901095.jpg";
import popular12 from "@/public/images/brandProducts/Wishbrand/4660147902078.jpg";
import popular13 from "@/public/images/brandProducts/Wishbrand/6922907405242.jpg";
import popular14 from "@/public/images/brandProducts/Wishbrand/6971289090119.jpg";
import popular15 from "@/public/images/brandProducts/Wishbrand/6971289090126.jpg";
import popular16 from "@/public/images/brandProducts/Wishbrand/6971289090133.jpg";
import popular17 from "@/public/images/brandProducts/Wishbrand/6971289090140.jpg";
import popular18 from "@/public/images/brandProducts/Wishbrand/oil.jpg";

export const WISH = "Wish";

export const wishNewImages = {
  "Соки и напитки": [
    {
      image: popular1,
    },
    {
      image: popular2,
    },
    {
      image: popular3,
    },
    {
      image: popular4,
    },
    {
      image: popular5,
    },
    {
      image: popular6,
    },
    {
      image: popular7,
    },
    {
      image: popular8,
    },
    {
      image: popular9,
    },
    {
      image: popular10,
    },
    {
      image: popular11,
    },
    {
      image: popular12,
    },
  ],
  "Консервированные грибы и овощи": [
    {
      image: popular13,
    },
    {
      image: popular14,
    },
    {
      image: popular15,
    },
    {
      image: popular16,
    },
    {
      image: popular17,
    },
  ],
  "Бульоны/Лапша": [
    {
      image: popular18,
    },
  ],
};
