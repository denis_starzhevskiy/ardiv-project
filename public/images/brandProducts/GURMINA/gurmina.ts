import popular1 from "@/public/images/brandProducts/GURMINA/4815477010225.jpg";
import popular2 from "@/public/images/brandProducts/GURMINA/4815477009250.jpg";
import popular3 from "@/public/images/brandProducts/GURMINA/4815477004439.jpg";
import popular4 from "@/public/images/brandProducts/GURMINA/4815477004408.jpg";
import popular5 from "@/public/images/brandProducts/GURMINA/4815477010164.jpg";
import popular6 from "@/public/images/brandProducts/GURMINA/4815477004446.jpg";
import popular7 from "@/public/images/brandProducts/GURMINA/4815477004453.jpg";
import popular8 from "@/public/images/brandProducts/GURMINA/4815477004521.jpg";
import popular9 from "@/public/images/brandProducts/GURMINA/4815477004538.jpg";
import popular10 from "@/public/images/brandProducts/GURMINA/4815477004576.jpg";
import popular11 from "@/public/images/brandProducts/GURMINA/4815477004613.jpg";
import popular12 from "@/public/images/brandProducts/GURMINA/4815477004620.jpg";
import popular13 from "@/public/images/brandProducts/GURMINA/4815477007041.jpg";
import popular14 from "@/public/images/brandProducts/GURMINA/4815477007065.jpg";
import popular15 from "@/public/images/brandProducts/GURMINA/4815477009236.jpg";
import popular16 from "@/public/images/brandProducts/GURMINA/4815477009267.jpg";
import popular17 from "@/public/images/brandProducts/GURMINA/4815477010119.jpg";
import popular18 from "@/public/images/brandProducts/GURMINA/4815477010157.jpg";
import popular19 from "@/public/images/brandProducts/GURMINA/4815477010218.jpg";

export const GURMINA = "Gurmina";

export const gurminaImages = {
  "Специи ": [
    {
      image: popular5,
    },
    {
      image: popular1,
    },
    {
      image: popular17,
    },
    {
      image: popular18,
    },
    {
      image: popular19,
    },
  ],
  "Приправы ": [
    {
      image: popular2,
    },
    {
      image: popular3,
    },
    {
      image: popular4,
    },
    {
      image: popular6,
    },
    {
      image: popular7,
    },
    {
      image: popular8,
    },
    {
      image: popular9,
    },
    {
      image: popular10,
    },
    {
      image: popular11,
    },
    {
      image: popular12,
    },
    {
      image: popular13,
    },
    {
      image: popular14,
    },
    {
      image: popular15,
    },
    {
      image: popular16,
    },
  ],
};
