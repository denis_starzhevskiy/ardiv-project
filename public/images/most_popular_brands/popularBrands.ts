// import popular1 from "@/public/images/most_popular_brands/popular1.jpg";
// import popular2 from "@/public/images/most_popular_brands/popular2.jpg";
// import popular3 from "@/public/images/most_popular_brands/popular3.jpg";
// import popular4 from "@/public/images/most_popular_brands/popular4.jpg";
// import popular5 from "@/public/images/most_popular_brands/popular5.jpg";
// import popular6 from "@/public/images/most_popular_brands/popular6.jpg";
// import popular7 from "@/public/images/most_popular_brands/popular7.jpg";
// import popular8 from "@/public/images/most_popular_brands/popular8.jpg";
// import popular9 from "@/public/images/most_popular_brands/popular9.jpg";
// import popular10 from "@/public/images/most_popular_brands/popular10.jpg";
// import popular11 from "@/public/images/most_popular_brands/popular11.jpg";

import popular1 from "@/public/images/most_popular_brands/popular1.jpg";
import popular3 from "@/public/images/brands/new_brands/djem_imperia.jpg";
import popular4 from "@/public/images/brands/new_brands/maximus.jpg";
import popular5 from "@/public/images/brands/new_brands/good_morning.jpg";
import popular6 from "@/public/images/brands/new_brands/london_tea.jpg";
import popular7 from "@/public/images/brands/new_brands/yarulo.jpg";
import popular13 from "@/public/images/most_popular_brands/popular13.png";
import popular14 from "@/public/images/most_popular_brands/popular14.png";
import popular15 from "@/public/images/most_popular_brands/popular15.jpg";
import popular16 from "@/public/images/most_popular_brands/popular16.png";
import popular17 from "@/public/images/most_popular_brands/popular17.jpg";
import popular18 from "@/public/images/most_popular_brands/popular18.jpg";
import popular19 from "@/public/images/most_popular_brands/popular19.png";
import popular20 from "@/public/images/most_popular_brands/popular20.jpg";
import popular21 from "@/public/images/most_popular_brands/popular21.jpg";
import popular22 from "@/public/images/most_popular_brands/popular22.jpg";
import popular23 from "@/public/images/most_popular_brands/popular23.jpg";
import popular24 from "@/public/images/most_popular_brands/popular24.png";
import popular25 from "@/public/images/most_popular_brands/popular26.png";
import { StaticImageData } from "next/image";
import { YARILO } from "@/public/images/brandProducts/YARILO/Yarilo";
import { AGEEVSKY } from "@/public/images/brandProducts/AGEEVSKY/AgeevskyBrand";
import { CANNEDMILK } from "@/public/images/brandProducts/CannedMILK/cannedMilkbrand";
import { IMPIREOFJAM } from "@/public/images/brandProducts/ImpireOfJAM/ImpireOfJam";
import { MERENGA } from "@/public/images/brandProducts/MERENGA/marenga";
import { MUCHSKINO } from "@/public/images/brandProducts/Muchskino/muchskino";
import { SNACCHIPS } from "@/public/images/brandProducts/SNACCHIPS/snacchips";
import { SWEETSOTHER } from "@/public/images/brandProducts/SweetsOther/sweetsOther";
import { MORSHINSKAYA } from "@/public/images/brandProducts/WaterMORSHINSKAYA/morshinskaya";
import { GOODMORNING } from "@/public/images/brandProducts/Goodmorning/goodmorning";
import { LONDONCLUB } from "@/public/images/brandProducts/LondonClub/londonClub";
import { MAXIMUS } from "@/public/images/brandProducts/maximuss/maximus";
import { WISH } from "@/public/images/brandProducts/Wishbrand/wish";
import { GURMINA } from "@/public/images/brandProducts/GURMINA/gurmina";
import logomuchskino from "@/public/images/brandProducts/Muchskino/logomuchskino.png";
import wishlogo from "@/public/images/brandProducts/Wishbrand/Wishbrandlogo.png";
import sweetlogo from "@/public/images/brandProducts/SweetsOther/logosweet.png";
import ageevskylogo from "@/public/images/brandProducts/AGEEVSKY/ageevskylogo.png";
import { ASCANIA } from "@/public/images/brandProducts/ASCANIA/ascania";
import { SINGOFCARE } from "@/public/images/brandProducts/SINGOFCARE/singOfCare";
import { BOCHKARI } from "@/public/images/brandProducts/Bochkari/bochkari";
import { SCANDIA } from "@/public/images/brandProducts/SCANDIA/scandia";
import { VORONZOVSKY } from "@/public/images/brandProducts/Voronzovsky/voronzovsky";
import { AMAPOLA } from "@/public/images/brandProducts/Amapola/amapola";
import { DONKO } from "@/public/images/brandProducts/DONKO/donko";
import { DELICATE } from "@/public/images/brandProducts/DELICATE/delicate";

export const OBOLON = "obolon";

export interface ImageType {
  image: StaticImageData;
  name?: string;
  link?: string;
  isNoImage?: boolean;
}

export const popularImages: ImageType[] = [
  {
    image: ageevskylogo,
    link: "https://obolon.ua/ua",
    name: AGEEVSKY,
  },
  {
    image: popular25,
    link: "https://obolon.ua/ua",
    name: DELICATE,
  },
  {
    image: popular24,
    link: "https://obolon.ua/ua",
    name: DONKO,
  },
  {
    image: popular23,
    link: "https://obolon.ua/ua",
    name: AMAPOLA,
  },
  {
    image: popular22,
    link: "https://obolon.ua/ua",
    name: VORONZOVSKY,
  },
  {
    image: popular21,
    link: "https://obolon.ua/ua",
    name: SCANDIA,
  },
  {
    image: popular20,
    link: "https://obolon.ua/ua",
    name: BOCHKARI,
  },
  {
    image: popular19,
    link: "https://obolon.ua/ua",
    name: SINGOFCARE,
  },
  {
    image: popular17,
    link: "https://obolon.ua/ua",
    name: GURMINA,
  },
  {
    image: popular18,
    link: "https://obolon.ua/ua",
    name: ASCANIA,
  },
  {
    image: popular15,
    link: "https://obolon.ua/ua",
    name: CANNEDMILK,
  },
  // {
  //   image: popular12,
  //   link: "https://obolon.ua/ua",
  //   name: CHOCOLAND
  // },
  {
    image: popular13,
    link: "https://obolon.ua/ua",
    name: MERENGA,
  },
  {
    image: logomuchskino,
    link: "https://obolon.ua/ua",
    name: MUCHSKINO,
  },

  {
    image: popular16,
    link: "https://obolon.ua/ua",
    name: SNACCHIPS,
  },
  {
    image: sweetlogo,
    link: "https://obolon.ua/ua",
    name: SWEETSOTHER,
  },
  {
    image: popular14,
    link: "https://obolon.ua/ua",
    name: MORSHINSKAYA,
  },
  {
    image: wishlogo,
    link: "https://obolon.ua/ua",
    name: WISH,
  },
  // {
  //   image: popular1,
  //   link: "https://obolon.ua/ua",
  //   name: OBOLON,
  // },
  // {
  //   image: popular2,
  //   link: "http://biscuit.com.ua/",
  //   name: KRUTON,
  // },
  {
    image: popular3,
    link: "http://maheev.ru/",
    name: IMPIREOFJAM,
  },
  {
    image: popular4,
    link: "http://www.makfa.ru/",
    name: MAXIMUS,
  },
  {
    image: popular5,
    link: "https://carlsbergukraine.com/",
    name: GOODMORNING,
  },
  {
    image: popular6,
    link: "https://khortytsa.com/en",
    name: LONDONCLUB,
  },
  {
    image: popular7,
    link: "https://www.nestle.ua/brands/culinary/mivina",
    name: YARILO,
  },
];
