export const fontTitleSize = "50px";

export const fontTitleWeight = 900;

export const primaryColor = "#f74e37";
export const secondaryColor = "#c23d2b";
