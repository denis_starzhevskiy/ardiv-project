import React from "react";
import { LinkProps, default as NextLink } from "next/link";
import styles from "./link.module.scss";
import clsx from "clsx";

type Props = Omit<
  React.AnchorHTMLAttributes<HTMLAnchorElement>,
  keyof LinkProps
> &
  LinkProps & {
    children?: React.ReactNode;
  } & React.RefAttributes<HTMLAnchorElement>;

const Link: React.FC<Props> = (props) => {
  return <NextLink {...props} className={clsx(styles.link, props.className)} />;
};

export default Link;
