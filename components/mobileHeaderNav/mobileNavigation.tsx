import React, { CSSProperties, useCallback, useRef, useState } from "react";
import s from "./mobileNavigation.module.scss";
import { Manager, Popper, Reference } from "react-popper";
import { useRouter } from "next/navigation";
import { FormattedMessage } from "react-intl";
import { IconButton } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import { LOCALES } from "@/public/i18/locales";
import UnitedKingdomFlagImage from "@/public/images/flags/United-Kingdom.png";
import IsraelFlagImage from "@/public/images/flags/Israel.png";
import Image from "next/image";

type Props = {
  currentLanguage: LOCALES;
  setLanguage: (newLanguage: LOCALES) => void;
  styles?: CSSProperties;
};

export const MobileMenu: React.FC<Props> = ({
  styles,
  currentLanguage,
  setLanguage,
}) => {
  const router = useRouter();
  const [dropdownOpen, setDropdownToggle] = useState(false);
  const dropdownListRef = useRef(null);
  const dropdownButtonRef = useRef(null);

  const setButtonRef = useCallback((node, ref) => {
    dropdownButtonRef.current = node;
    return ref(node);
  }, []);

  const setListRef = useCallback((node, ref) => {
    dropdownListRef.current = node;
    return ref(node);
  }, []);

  const dropdownToggle = () => {
    setDropdownToggle(!dropdownOpen);
  };

  const modifiers = {
    preventOverflow: {
      padding: 0,
    },
    shift: {
      enabled: true,
    },
    flip: {
      enabled: true,
      flipVariationsByContent: true,
      behavior: "flip",
    },
  };

  return (
    <Manager>
      <div>
        <Reference>
          {({ ref }) => (
            <IconButton
              onMouseEnter={dropdownToggle}
              onMouseLeave={dropdownToggle}
              ref={(node) => setButtonRef(node, ref)}
              style={{
                display: "flex",
                alignItems: "center",
                height: "50px",
                cursor: "pointer",
              }}
            >
              <MenuIcon />
              {dropdownOpen && (
                <Popper placement="bottom" modifiers={modifiers}>
                  {({ ref, style, placement }) => (
                    <ul
                      ref={(node) => setListRef(node, ref)}
                      style={{ ...style, ...styles }}
                      data-placement={placement}
                      className={s.menu}
                    >
                      <li onClick={() => router.push("/")}>ARDIV LTD</li>
                      <li onClick={() => router.push("/about-us")}>
                        <FormattedMessage id={"ul_menu_option_2"} />
                      </li>
                      <li onClick={() => router.push("/transport-logistic")}>
                        <FormattedMessage id={"ul_menu_option_3"} />
                      </li>
                      <li onClick={() => router.push("/warehouse-logistic")}>
                        <FormattedMessage id={"ul_menu_option_4"} />
                      </li>
                      <li onClick={() => router.push("/brands")}>
                        <FormattedMessage id={"ul_own_brends"} />
                      </li>
                      <li onClick={() => router.push("/contacts")}>
                        <FormattedMessage id={"ul_contacts"} />
                      </li>
                      <li
                        style={{
                          display: "flex",
                          columnGap: "10px",
                          alignItems: "center",
                          justifyContent: "space-evenly",
                        }}
                      >
                        <p
                          onClick={() => setLanguage(LOCALES.ENGLISH)}
                          style={
                            currentLanguage.toString().split("-")[0] === "en"
                              ? { color: "#f74e37" }
                              : { color: "black" }
                          }
                        >
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              gap: 10,
                            }}
                          >
                            <Image
                              src={UnitedKingdomFlagImage}
                              alt={"United Kingdom flag"}
                            />
                            <div>en</div>
                          </div>
                        </p>
                        <p
                          onClick={() => setLanguage(LOCALES.RUSSIAN)}
                          style={
                            currentLanguage.toString().split("-")[0] === "ru"
                              ? { color: "#f74e37" }
                              : { color: "black" }
                          }
                        >
                          ru
                        </p>
                        <p
                          onClick={() => setLanguage(LOCALES.HEBREW)}
                          style={
                            currentLanguage.toString().split("-")[0] === "he"
                              ? { color: "#f74e37" }
                              : { color: "black" }
                          }
                        >
                          <div
                            style={{
                              display: "flex",
                              alignItems: "center",
                              gap: 10,
                            }}
                          >
                            <Image src={IsraelFlagImage} alt={"Israel flag"} />
                            <div>he</div>
                          </div>
                        </p>
                      </li>
                      {/*<li*/}
                      {/*  onClick={() => setLanguage(LOCALES.ENGLISH)}*/}
                      {/*  style={*/}
                      {/*    currentLanguage.split("-")[0] === "en"*/}
                      {/*      ? { color: "#f74e37" }*/}
                      {/*      : {}*/}
                      {/*  }*/}
                      {/*>*/}
                      {/*  <FormattedMessage id={"english_language"} />*/}
                      {/*</li>*/}
                      {/*<li*/}
                      {/*  onClick={() => setLanguage(LOCALES.RUSSIAN)}*/}
                      {/*  style={*/}
                      {/*    currentLanguage.split("-")[0] === "ru"*/}
                      {/*      ? { color: "#f74e37" }*/}
                      {/*      : {}*/}
                      {/*  }*/}
                      {/*>*/}
                      {/*  <FormattedMessage id={"russian_language"} />*/}
                      {/*</li>*/}
                      {/*<li*/}
                      {/*  onClick={() => setLanguage(LOCALES.HEBREW)}*/}
                      {/*  style={*/}
                      {/*    currentLanguage.split("-")[0] === "he"*/}
                      {/*      ? { color: "#f74e37" }*/}
                      {/*      : {}*/}
                      {/*  }*/}
                      {/*>*/}
                      {/*  <FormattedMessage id={"hebrew_language"} />*/}
                      {/*</li>*/}
                    </ul>
                  )}
                </Popper>
              )}
            </IconButton>
          )}
        </Reference>
      </div>
    </Manager>
  );
};
