"use client";
import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Navigation } from "swiper";
import React, { useRef, useState } from "react";

// import hortica from "@/public/images/brands/khortica.jpg";
// import monteazul from "@/public/images/brands/monteazul.jpg";
// import donroberto from "@/public/images/brands/donroberto.jpg";
// import bribon from "@/public/images/brands/bribon.jpg";
// import garnishiland from "@/public/images/brands/garnishiland.jpg";
// import cityoflondon from "@/public/images/brands/cityoflondon.jpg";
// import eightislands from "@/public/images/brands/eightislands.jpg";
// import morosha from "@/public/images/brands/morosha.jpg";
// import pogues from "@/public/images/brands/pogues.jpg";
// import shustov from "@/public/images/brands/shustov.jpg";
// import westkork from "@/public/images/brands/westkork.jpg";
// import crabie from "@/public/images/brands/crabie.jpg";

import hortica from "@/public/images/brands/new_brands/ardiv.jpg";
import monteazul from "@/public/images/brands/new_brands/djem_imperia.jpg";
import donroberto from "@/public/images/brands/new_brands/good_morning.jpg";
import bribon from "@/public/images/brands/new_brands/svk.jpg";
import garnishiland from "@/public/images/brands/new_brands/maximus.jpg";
import cityoflondon from "@/public/images/brands/new_brands/london_tea.jpg";
import eightislands from "@/public/images/brands/new_brands/yarulo.jpg";

import Image from "next/image";
import s from "./swiper.module.scss";
import { IconButton } from "@mui/material";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";

const brands = [
  {
    logo: hortica,
    link: "https://khortytsa.com/en",
  },
  {
    logo: monteazul,
    link: "",
  },
  {
    logo: donroberto,
    link: "",
  },
  {
    logo: bribon,
    link: "",
  },
  {
    logo: garnishiland,
    link: "https://www.westcorkdistillers.com/",
  },
  {
    logo: cityoflondon,
    link: "",
  },
  {
    logo: eightislands,
    link: "",
  },
];

export const DesktopSwiperContainer = () => {
  const navigationPrevRef = useRef<HTMLButtonElement>(null);
  const navigationNextRef = useRef<HTMLButtonElement>(null);
  const [isEnd, setIsEnd] = useState(false);
  const [isBeginning, setIsBeginning] = useState(true);

  const swiperPrevNextButton = {
    zIndex: 10,
    position: "absolute",
    top: "50%",
    transform: "translateY(-50%)",
    background: "white",
    boxShadow: "rgba(149, 157, 165, 0.2) 0px 8px 24px",
    "&:hover": {
      background: "white",
    },
    "&.Mui-disabled": {
      background: "white",
    },
  };

  return (
    <Swiper
      modules={[Autoplay, Navigation]}
      slidesPerView={3}
      spaceBetween={50}
      autoplay={{
        delay: 1000,
        disableOnInteraction: false,
      }}
      onSlideChange={(swiper) => {
        setIsBeginning(swiper.isBeginning);
        setIsEnd(swiper.isEnd);
      }}
      onBeforeInit={(swiper) => {
        const { navigation } = swiper.params;
        if (navigation && typeof navigation === "object") {
          navigation.prevEl = navigationPrevRef.current;
          navigation.nextEl = navigationNextRef.current;
        }
      }}
      className={s.swiper}
    >
      {brands.map((brand, idx) => (
        <SwiperSlide
          key={idx}
          style={{ cursor: "pointer" }}
          className={s.swiperSlide}
        >
          <div className={s.imageContainer}>
          <Image
            src={brand.logo}
            alt={"logo"}
            width={200}
          />
          </div>
        </SwiperSlide>
      ))}
      <IconButton
        disabled={isBeginning}
        sx={{ ...swiperPrevNextButton, left: 10 }}
        ref={navigationPrevRef}
        aria-label="swiper prev button"
        size={"small"}
      >
        <ChevronLeftIcon fontSize={"small"} />
      </IconButton>
      <IconButton
        disabled={isEnd}
        sx={{ ...swiperPrevNextButton, right: 10 }}
        ref={navigationNextRef}
        aria-label="swiper next button"
        size={"small"}
      >
        <ChevronRightIcon fontSize={"small"} />
      </IconButton>
    </Swiper>
  );
};
