import React from "react";

import s from "./flipCard.module.scss";
import {useRouter} from "next/navigation";

type FlipCardProps = {
  image: React.ReactNode;
  label: string;
  link: string;
  width?: string;
  height?: string;
};

export const FlipCard = ({
  image,
  label,
  width,
  height,
}: FlipCardProps) => {
  const router = useRouter()
  return (
    <div className={s.flipCard} style={{ height: height, width: width }}>
      <div className={s.flipCardInner}>
        <div className={s.flipCardFront}>{image}</div>
        <div className={s.flipCardBack}>
          <p className={s.backTitle}>{label}</p>
          <p className={s.link} onClick={() => router.push('/about-us')}>
            Перейти на сайт
          </p>
        </div>
      </div>
    </div>
  );
};
