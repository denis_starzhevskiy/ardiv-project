import React from "react";
import styles from "./percentage-range.module.scss";

type Props = {
  // from 0 to 100
  value: number;
  title: string;
};

const PercentageRange: React.FC<Props> = ({ value, title }) => {
  return (
    <>
      <div>
        <h4 className={styles.title}>
          {title} <span className={styles.percentageLabel}>{value}%</span>
        </h4>
      </div>
      <div className={styles.range}>
        <div
          className={styles.rangeTrack}
          style={{ width: `${Math.min(Math.max(0, value), 100)}%` }}
        ></div>
      </div>
    </>
  );
};

export default PercentageRange;
