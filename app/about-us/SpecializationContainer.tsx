import s from "@/app/about-us/styles.module.scss";
import PercentageRange from "@/components/PercentageRange/PercentageRange";
import NextLink from "next/link";
import React from "react";
import { FormattedMessage, useIntl } from "react-intl";

const SpecializationContainer = () => {
  const intl = useIntl();

  const specializations = [
    { title: intl.formatMessage({ id: "aboutUsSpecialization1" }), value: 40 },
    { title: intl.formatMessage({ id: "aboutUsSpecialization2" }), value: 25 },
    { title: intl.formatMessage({ id: "aboutUsSpecialization3" }), value: 10 },
    { title: intl.formatMessage({ id: "aboutUsSpecialization4" }), value: 5 },
    { title: intl.formatMessage({ id: "aboutUsSpecialization5" }), value: 10 },
  ];

  return (
    <section className={s.specializationContainer}>
      <h3 className={s.specializationTitle}>
        <FormattedMessage id={"aboutUsSpecializationTitle"} />
      </h3>
      <div className={s.specializationSection}>
        <div>
          {specializations.map((specialization) => (
            <PercentageRange
              key={specialization.title}
              value={specialization.value}
              title={specialization.title}
            />
          ))}
        </div>
        <div className={s.professionalismSection}>
          <p className={s.hundredPercentProfessionalism}>100%</p>
          <p className={s.professionalismLabel}>
            <FormattedMessage id={"aboutUsProfessionalism"} />
          </p>
          <NextLink href={"/contacts"} className={s.contactButton}>
            <FormattedMessage id={"aboutUsGetInTouchButton"} />
          </NextLink>
        </div>
      </div>
    </section>
  );
};

export default SpecializationContainer;
