"use client";

import React from "react";
import s from "@/app/about-us/styles.module.scss";
import SpecializationContainer from "@/app/about-us/SpecializationContainer";
import AboutUsContainer from "@/app/about-us/AboutUsContainer";
import ReliableClientsContainer from "@/app/about-us/ReliableClientsContainer";

const Page = () => {
  return (
    <div className={s.container}>
      <SpecializationContainer />
      <AboutUsContainer />
      <ReliableClientsContainer />
    </div>
  );
};

export default Page;
