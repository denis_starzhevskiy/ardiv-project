import s from "@/app/about-us/styles.module.scss";
import React from "react";
import Image from "next/image";
import client1 from "@/public/images/clients/client1.jpg";
import client2 from "@/public/images/clients/client2.jpg";
import client3 from "@/public/images/clients/client3.jpg";
import client4 from "@/public/images/clients/client4.jpg";
import client5 from "@/public/images/clients/client5.jpg";
import client9 from "@/public/images/clients/client9.jpg";
import client10 from "@/public/images/clients/client10.jpg";
import client11 from "@/public/images/clients/client11.jpg";
import client12 from "@/public/images/clients/client12.jpg";
import client13 from "@/public/images/clients/client14.jpg"
import { FormattedMessage } from "react-intl";

const clientsImages = [
  client1,
  client2,
  client3,
  client4,
  client5,
  client9,
  client10,
  client11,
  client12,
  client13
];

const ReliableClientsContainer = () => {
  return (
    <div className={s.reliableClientsContainer}>
      <h3 className={s.reliableClientsTitle}>
        <FormattedMessage id={"theMostReliableClients"} />
      </h3>
      <div className={s.reliableClientsSection}>
        {clientsImages.map((image, i) => (
          <div key={i} className={s.reliableClientsSectionItem}>
            <Image src={image} alt={`client ${i}`} width={300} height={i === 9 ? 200 : 120}/>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ReliableClientsContainer;
