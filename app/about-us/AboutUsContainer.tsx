import s from "@/app/about-us/styles.module.scss";
import React from "react";
import Link from "@/components/Link/Link";
import Image from "next/image";
import ikra1Image from "@/public/images/ikra_1.jpg";
import { FormattedMessage } from "react-intl";

const AboutUsContainer = () => {
  return (
    <section className={s.aboutUsContainer}>
      <h3 className={s.aboutUsTitle}>
        <FormattedMessage id={"aboutUs"} />
      </h3>
      <div className={s.aboutUsSection}>
        <div>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage id={"aboutUsDescription1"} />
          </p>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage id={"aboutUsDescription2"} />
          </p>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage
              id={"aboutUsDescription3"}
              values={{
                Link: (
                  <Link href={"/sales-structure"}>
                    <FormattedMessage id={"aboutUsDescription3Link"} />
                  </Link>
                ),
              }}
            />
          </p>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage
              id={"aboutUsDescription4"}
              values={{
                Link: (
                  <Link href={"/warehouse-logistic"}>
                    <FormattedMessage id={"aboutUsDescription4Link"} />
                  </Link>
                ),
              }}
            />
          </p>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage
              id={"aboutUsDescription5"}
              values={{
                Link: (
                  <Link href={"/transport-logistic"}>
                    <FormattedMessage id={"aboutUsDescription5Link"} />
                  </Link>
                ),
              }}
            />
          </p>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage id={"aboutUsDescription6"} />
          </p>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage id={"aboutUsDescription7"} />
          </p>
          <p className={s.aboutUsParagraph}>
            <FormattedMessage id={"aboutUsDescription8"} />
          </p>
        </div>
        <div className={s.aboutUsImageContainer}>
          <Image
            width={600}
            height={400}
            src={ikra1Image}
            alt="about us image"
          />
        </div>
      </div>
    </section>
  );
};

export default AboutUsContainer;
