"use client";
import React from "react";

import s from "./transportLayout.module.scss";
import { FormattedMessage } from "react-intl";

const Layout = ({ children }) => {
  return (
    <>
      <header className={s.header}>
        <FormattedMessage id={"ul_menu_option_3"}>
          {(value) => {
            return <p className={s.headerTitle}>{value}</p>;
          }}
        </FormattedMessage>
        <FormattedMessage id={"full_name"}>
          {(value) => {
            return <p className={s.headerSubTitle}>{value}</p>;
          }}
        </FormattedMessage>
      </header>
      {children}
    </>
  );
};

export default Layout;
