"use client";

import React from "react";
import Image from "next/image";
import cars from "@/public/images/car.png";
import s from "@/app/transport-logistic/transportLayout.module.scss";
import { FormattedMessage } from "react-intl";

const Page = () => {
  return (
    <div className={s.mainContainer}>
      <Image src={cars} alt={"cars"} className={s.carImage} />
      <div>
        <FormattedMessage id={"ul_menu_option_3"}>
          {(value) => {
            return <p className={s.brandsTitle}>{value}</p>;
          }}
        </FormattedMessage>
        <FormattedMessage
          id={"transport_logistic_description"}
          values={{
            a: (chunks) => {
              return <a className={s.link}>{chunks}</a>;
            },
          }}
        >
          {(value) => {
            return <p className={s.text}>{value}</p>;
          }}
        </FormattedMessage>
      </div>
    </div>
  );
};

export default Page;
