import React, { useCallback, useRef, useState } from "react";
import {
  Manager,
  Popper,
  Reference,
  PopperProps,
  Modifier,
} from "react-popper";
import s from "@/app/layout.module.scss";
import { IconButton } from "@mui/material";
import { Placement } from "@popperjs/core";

type Props = {
  buttonText: React.ReactNode;
  options: {
    name: string;
    onClick: () => void;
    checkParam: string;
    label: string | React.ReactNode;
  }[];
  value: string;
  styles?: React.CSSProperties;
  labelIcon?: React.ReactNode;
  placement?: Placement;
};

export const DropdownItem: React.FC<Props> = ({
  buttonText,
  options,
  value,
  styles,
  labelIcon,
  placement,
}) => {
  const [dropdownOpen, setDropdownToggle] = useState(false);
  const dropdownListRef = useRef(null);
  const dropdownButtonRef = useRef(null);

  const setButtonRef = useCallback((node, ref) => {
    dropdownButtonRef.current = node;
    return ref(node);
  }, []);

  const setListRef = useCallback((node, ref) => {
    dropdownListRef.current = node;
    return ref(node);
  }, []);

  const dropdownToggle = () => {
    setDropdownToggle(!dropdownOpen);
  };

  const modifiers = {
    preventOverflow: {
      padding: 0,
    },
    shift: {
      enabled: true,
    },
    flip: {
      enabled: true,
      flipVariationsByContent: true,
      behavior: "flip",
    },
  };

  return (
    <Manager>
      <Reference>
        {({ ref }) => (
          <div
            onMouseEnter={dropdownToggle}
            onMouseLeave={dropdownToggle}
            ref={(node) => setButtonRef(node, ref)}
            style={{
              display: "flex",
              alignItems: "center",
              height: "50px",
              cursor: "pointer",
            }}
          >
            {labelIcon ? <IconButton>{labelIcon}</IconButton> : buttonText}
            {dropdownOpen && (
              <Popper placement={placement} modifiers={modifiers}>
                {({ ref, style, placement }) => (
                  <ul
                    ref={(node) => setListRef(node, ref)}
                    style={{ ...style, ...styles }}
                    data-placement={placement}
                    className={s.menu}
                  >
                    {options.map((item) => {
                      return (
                        <li
                          key={item.name}
                          onClick={item.onClick}
                          style={
                            value === item.checkParam
                              ? { color: "#f74e37" }
                              : {}
                          }
                        >
                          {item.label}
                        </li>
                      );
                    })}
                  </ul>
                )}
              </Popper>
            )}
          </div>
        )}
      </Reference>
    </Manager>
  );
};
