import { NextRequest, NextResponse } from "next/server";
import mailService from "@/services/mail";

export async function POST(req: NextRequest) {
  const { firstName, phone, email, message } = await req.json();
  if (!firstName || !email || !message) {
    return new Response(
      JSON.stringify({
        error: "firstName, email, message fields are required",
      }),
      { status: 400 }
    );
  }

  try {
    await mailService.sendMail({
      from: "Ardiv <romantokar.biz@gmail.com>",
      to: "yulia@ardiv.co.il",
      subject: "Ardiv - Contact Us",
      html: `<div><h4>Имя</h4><p>${firstName}</p></div>${
        phone ? `<div><h4>Телефон</h4><p>${phone}</p></div>` : ""
      }<div><h4>Почта</h4><p>${email}</p></div><div><h4>Сообщение</h4><p>${message}</p></div>`,
    });
    return NextResponse.json({ message: "success" });
  } catch (e) {
    return NextResponse.json({ error: "something went wrong" });
  }
}
