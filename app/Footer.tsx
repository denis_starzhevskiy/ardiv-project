import s from "@/app/layout.module.scss";
import Image from "next/image";
import logoImage from "@/public/images/logo.png";
import React from "react";
import { FormattedMessage } from "react-intl";

export const Footer = () => {
  return (
    <>
      <div className={s.footer}>
        <Image src={logoImage} alt={"logo"} width={200} />
        <div className={s.footerInfo}>
          <FormattedMessage id={"ul_contacts"} />
          <hr className={s.divider} />
          <ul className={s.contactsFooterList}>
            <li>
              <FormattedMessage id={"email"}>
                {(value) => {
                  return <p>{value + ": office@ardiv.co.il"}</p>;
                }}
              </FormattedMessage>
            </li>
            <li>
              <FormattedMessage id={"phone"}>
                {(value) => {
                  return <p>{value + ": +9724-8581792"}</p>;
                }}
              </FormattedMessage>
            </li>
            <li>
              <FormattedMessage id={"address"}>
                {(value) => {
                  return <p>{value + ": Аба Хуши 149 Хайфа"}</p>;
                }}
              </FormattedMessage>
            </li>
          </ul>
        </div>
      </div>
      <div className={s.rightFooterContainer}>
        <FormattedMessage id={"all_rights"}>
          {(value) => {
            return <p>{`© 2023 ARDIV LTD. ${value}`}</p>;
          }}
        </FormattedMessage>
      </div>
    </>
  );
};
