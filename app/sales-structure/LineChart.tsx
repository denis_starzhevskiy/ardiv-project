"use client";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);
import s from "./styles.module.scss";
import { useIntl } from "react-intl";

const LineChart = () => {
  const intl = useIntl();

  const labels = new Array(6)
    .fill(0)
    .map(
      (_, i) =>
        intl.formatMessage({
          id: `salesStructureLineChartYAxisLabel${i + 1}`,
        }) as string
    );

  const lineChartLabel = intl.formatMessage({
    id: "salesStructureLineChartLabel",
  });

  return (
    <div className={s.lineChartContainer}>
      <div>
        <Line
          options={{
            responsive: true,
            plugins: {
              legend: {
                position: "bottom",
              },
            },
          }}
          data={{
            labels,
            datasets: [
              {
                label: lineChartLabel,
                data: [40, 60, 250, 300, 300, 500],
                borderColor: "rgb(255, 99, 132)",
                backgroundColor: "rgba(255, 99, 132, 0.5)",
              },
            ],
          }}
        />
      </div>
    </div>
  );
};
export default LineChart;
