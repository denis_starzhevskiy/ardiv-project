"use client";
import React from "react";
import s from "./styles.module.scss";
import { Section1 } from "@/app/sales-structure/Section1";
import Link from "@/components/Link/Link";
import PieChart from "@/app/sales-structure/PieChart";
import LineChart from "@/app/sales-structure/LineChart";
import { FormattedMessage } from "react-intl";

const Page = () => {
  return (
    <main className={s.container}>
      <Section1 />
      <section className={s.max1200Container}>
        <h2 className={s.sectionTitle}>
          <FormattedMessage id={"salesStructureDistributionTitle"} />
        </h2>
        <p className={s.sectionText}>
          <FormattedMessage
            id={"salesStructureDistributionDescription"}
            values={{
              Link1: (
                <Link href={"/"}>
                  <FormattedMessage
                    id={"salesStructureDistributionDescriptionLink1"}
                  />
                </Link>
              ),
              Link2: (
                <Link href={"/about-us"}>
                  <FormattedMessage
                    id={"salesStructureDistributionDescriptionLink2"}
                  />
                </Link>
              ),
            }}
          />
        </p>
        <PieChart />
      </section>
      <section className={s.max1200Container}>
        <h2 className={s.sectionTitle}>
          <FormattedMessage id={"salesStructureServedAreasTitle"} />
        </h2>
        <p className={s.sectionText}>
          <FormattedMessage id={"salesStructureServedAreasDescription"} />
        </p>
        <LineChart />
      </section>
    </main>
  );
};

export default Page;
