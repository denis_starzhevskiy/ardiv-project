"use client";
import { ArcElement, Chart as ChartJS, Legend, Tooltip, Title } from "chart.js";
import { Pie } from "react-chartjs-2";
import React from "react";
import s from "./styles.module.scss";
import { useIntl } from "react-intl";

ChartJS.register(ArcElement, Tooltip, Legend, Title);

const PieChart = () => {
  const intl = useIntl();

  const labels = new Array(3)
    .fill(0)
    .map((_, i) =>
      intl.formatMessage({ id: `salesStructurePieChartLabel${i + 1}` })
    );

  return (
    <div className={s.pieChartContainer}>
      <div>
        <Pie
          options={{
            plugins: {
              title: {
                display: true,
                text: intl.formatMessage({ id: "salesStructurePieChartTitle" }),
              },
              tooltip: {
                callbacks: {
                  label: function (context) {
                    const data = context.dataset.data[context.dataIndex];
                    return `${data}%`;
                  },
                },
              },
            },
          }}
          data={{
            labels: labels,
            datasets: [
              {
                data: [45, 40, 15],
                backgroundColor: [
                  "rgb(105, 189, 67)",
                  "rgb(101, 118, 194)",
                  "rgb(204, 33, 73)"
                ],
                borderWidth: 1,
              },
            ],
          }}
        />
      </div>
    </div>
  );
};

export default PieChart;
