import s from "@/app/sales-structure/styles.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHandshake } from "@fortawesome/free-regular-svg-icons";
import React from "react";
import { faBriefcase, faGlobeAsia } from "@fortawesome/free-solid-svg-icons";
import { FormattedMessage } from "react-intl";

const values = [
  {
    icon: faHandshake,
    title: <FormattedMessage id={"salesStructureSection1Cell1Title"} />,
    text: <FormattedMessage id={"salesStructureSection1Cell1Subtitle"} />,
  },
  {
    icon: faGlobeAsia,
    title: <FormattedMessage id={"salesStructureSection1Cell2Title"} />,
    text: <FormattedMessage id={"salesStructureSection1Cell2Subtitle"} />,
  },
  {
    icon: faBriefcase,
    title: <FormattedMessage id={"salesStructureSection1Cell3Title"} />,
    text: <FormattedMessage id={"salesStructureSection1Cell3Subtitle"} />,
  },
];

export const Section1 = () => {
  return (
    <section className={s.section1}>
      <div className={s.max1200Container}>
        <div className={s.section1GridContainer}>
          {values.map(({ icon, title, text }, index) => (
            <div key={index} className={s.section1GridContainerItem}>
              <div className={s.section1GridContainerItemIconContainer}>
                <FontAwesomeIcon
                  className={s.section1GridContainerItemIcon}
                  icon={icon}
                />
              </div>
              <div>
                <h5 className={s.section1GridContainerItemTitle}>{title}</h5>
                <p className={s.section1GridContainerItemText}>{text}</p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};
