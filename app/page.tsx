"use client";
import styles from "./page.module.scss";
import Image from "next/image";
import map from "../public/images/mainMap.jpg";
import { FormattedMessage } from "react-intl";
import {
  fontTitleSize,
  fontTitleWeight,
  primaryColor,
} from "@/public/styles/consts";
import ShoppingBasketIcon from "@mui/icons-material/ShoppingBasket";
import PublicIcon from "@mui/icons-material/Public";
import officeImage from "@/public/images/advantages.webp";
import { IconButton, useMediaQuery } from "@mui/material";
import ManIcon from "@mui/icons-material/AccessibilityNew";
import FavoriteIcon from "@mui/icons-material/Favorite";
import AccessTimeIcon from "@mui/icons-material/AccessTime";
import CodeIcon from "@mui/icons-material/Code";
import "swiper/css";
import "swiper/css/navigation";
import "swiper/css/pagination";
import "swiper/css/scrollbar";
import { DesktopSwiperContainer } from "@/components/swiper/DesktopSwiperContainer";
import ourClients from "@/public/images/our_clients.jpg";
import { clientsIImages } from "@/public/images/clients/images";
import { productsIImages } from "@/public/images/products/products";
import { FlipCard } from "@/components/flipCard/flipCard";

const getIconByIndex = (index) => {
  if (index === "one") {
    return (
      <IconButton sx={{ background: primaryColor }}>
        <ManIcon sx={{ color: "white" }} />
      </IconButton>
    );
  }

  if (index === "second") {
    return (
      <IconButton sx={{ background: primaryColor }}>
        <FavoriteIcon sx={{ color: "white" }} />
      </IconButton>
    );
  }

  if (index === "third") {
    return (
      <IconButton sx={{ background: primaryColor }}>
        <AccessTimeIcon sx={{ color: "white" }} />
      </IconButton>
    );
  }

  if (index === "fourth") {
    return (
      <IconButton sx={{ background: primaryColor }}>
        <CodeIcon sx={{ color: "white" }} />
      </IconButton>
    );
  }
};

export default function Home() {
  const mdUp = useMediaQuery("(min-width:900px)", { noSsr: true });
  return (
    <main className={styles.main}>
      <div id={"presentation_block"} className={styles.presentationBlock}>
        <div className={styles.presentationBlockTitle}>
          <FormattedMessage
            id={"mainTitle"}
            values={{
              b: (chunks) => {
                return <p style={{ color: "#f74e37" }}>{chunks}</p>;
              },
            }}
          >
            {(value) => (
              <p
                style={{
                  fontSize: fontTitleSize,
                  fontWeight: fontTitleWeight,
                  textTransform: "uppercase",
                }}
              >
                {value}
              </p>
            )}
          </FormattedMessage>
          <FormattedMessage id={"slogan"}>
            {(value) => {
              return <p className={styles.slogan}>{value}</p>;
            }}
          </FormattedMessage>
        </div>
        <Image src={map} alt={"map"} width={300} height={700} />
      </div>

      <div id={"description_block"} className={styles.descriptionBlock}>
        <p className={styles.descriptionCompanyName}>ARDIV LTD</p>
        <FormattedMessage
          id={"mainTitle"}
          values={{
            b: (chunks) => {
              return chunks;
            },
          }}
        >
          {(value) => {
            return (
              <p
                style={{
                  fontSize: "25px",
                  textTransform: "uppercase",
                  textAlign: "center",
                }}
              >
                {value}
              </p>
            );
          }}
        </FormattedMessage>

        <div id={"card-items"} className={styles.cardItemsBlock}>
          <div className={styles.cardItem}>
            <ShoppingBasketIcon className={styles.cardIcon} />
            <FormattedMessage id={"card_one_item_title"}>
              {(value) => {
                return <p className={styles.cardTitle}>{value}</p>;
              }}
            </FormattedMessage>
            <FormattedMessage
              id={"card_one_item_description"}
              values={{
                a: (chunks) => {
                  return (
                    <a href={"/catalog"} style={{ color: primaryColor }}>
                      {chunks}
                    </a>
                  );
                },
              }}
            >
              {(value) => {
                return <p className={styles.cardDescription}>{value}</p>;
              }}
            </FormattedMessage>
          </div>
          <div className={styles.cardItem}>
            <PublicIcon className={styles.cardIcon} />
            <FormattedMessage id={"card_second_item_title"}>
              {(value) => {
                return <p className={styles.cardTitle}>{value}</p>;
              }}
            </FormattedMessage>
            <FormattedMessage
              id={"card_second_item_description"}
              values={{
                a: (chunks) => {
                  return (
                    <a href={"/catalog"} style={{ color: primaryColor }}>
                      {chunks}
                    </a>
                  );
                },
              }}
            >
              {(value) => {
                return <p className={styles.cardDescription}>{value}</p>;
              }}
            </FormattedMessage>
          </div>
          <div className={styles.cardItem}>
            <p style={{ color: primaryColor, fontWeight: 700 }}>ТМ</p>
            <FormattedMessage id={"card_third_item_title"}>
              {(value) => {
                return <p className={styles.cardTitle}>{value}</p>;
              }}
            </FormattedMessage>
            <FormattedMessage
              id={"card_third_item_description"}
              values={{
                a: (chunks) => {
                  return (
                    <a href={"/catalog"} style={{ color: primaryColor }}>
                      {chunks}
                    </a>
                  );
                },
              }}
            >
              {(value) => {
                return <p className={styles.cardDescription}>{value}</p>;
              }}
            </FormattedMessage>
          </div>
        </div>
      </div>

      <div id={"advantages-block"} className={styles.advantagesBlock}>
        <Image src={officeImage} alt={"image"} width={mdUp ? 600 : 350} />
        <div className={styles.advantagesDescription}>
          <FormattedMessage id={"advantages_title"}>
            {(value) => {
              return <p className={styles.titleText}>{value}</p>;
            }}
          </FormattedMessage>
          <div className={styles.advantagesItems}>
            {["one", "second", "third", "fourth"].map((item, key) => {
              return (
                <div key={key}>
                  <div className={styles.advantagesItemTitleContainer}>
                    {getIconByIndex(item)}
                    <FormattedMessage id={`advantages_${item}_option_title`}>
                      {(value) => {
                        return (
                          <p className={styles.advantagesItemTitle}>{value}</p>
                        );
                      }}
                    </FormattedMessage>
                  </div>
                  <FormattedMessage
                    id={`advantages_${item}_option_description`}
                  >
                    {(value) => {
                      return (
                        <p className={styles.advantagesItemDescription}>
                          {value}
                        </p>
                      );
                    }}
                  </FormattedMessage>
                </div>
              );
            })}
          </div>
        </div>
      </div>

      <div id={"swiper-brands"} className={styles.swiperBrandsContainer}>
        <FormattedMessage id={"swiper_brands_title"}>
          {(value) => {
            return <p className={styles.titleText}>{value}</p>;
          }}
        </FormattedMessage>
        <DesktopSwiperContainer />
      </div>

      <div id={"clients-container"} className={styles.advantagesBlock}>
        <div className={styles.clientsContainer}>
          <FormattedMessage id={"clients_title"}>
            {(value) => {
              return <p className={styles.titleText}>{value}</p>;
            }}
          </FormattedMessage>
          <div className={styles.clientsImagesContainer}>
            {clientsIImages.map((item, key) => (
              <Image
                key={key}
                src={item}
                alt={"client1"}
                width={180}
                height={key === clientsIImages.length - 1 ? 100 : 70}
              />
            ))}
          </div>
        </div>
        <Image src={ourClients} alt={"employees"} width={600} />
      </div>

      <div id={"primary-products"} className={styles.primaryProducts}>
        <FormattedMessage id={"products_title"}>
          {(value) => {
            return <p className={styles.titleText}>{value}</p>;
          }}
        </FormattedMessage>
        <div className={styles.clientsProductsContainer}>
          {productsIImages.map((item, key) => (
            <FlipCard
              key={key}
              height={"250px"}
              label={item.label}
              link={item.link}
              image={
                <Image
                  key={key}
                  src={item.image}
                  alt={"client1"}
                  style={{
                    borderRadius: "20px",
                    width: "100%",
                    objectFit: "cover",
                    maxHeight: "250px",
                  }}
                />
              }
            />
          ))}
        </div>
      </div>
    </main>
  );
}
