"use client";
import React from "react";

import s from "./transportLayout.module.scss";
import { FormattedMessage } from "react-intl";

const Layout = ({ children }: { children: React.ReactNode }) => {
  return (
    <div>
      <header className={s.header}>
        <FormattedMessage id={"ul_menu_option_4"}>
          {(value) => {
            return <p className={s.headerTitle}>{value}</p>;
          }}
        </FormattedMessage>
        <FormattedMessage id={"full_name"}>
          {(value) => {
            return <p className={s.headerSubTitle}>{value}</p>;
          }}
        </FormattedMessage>
      </header>
      {children}
    </div>
  );
};

export default Layout;
