"use client";

import React from "react";
import Image from "next/image";
import sklad from "@/public/images/sklad.png";
import s from "@/app/transport-logistic/transportLayout.module.scss";
import { FormattedMessage } from "react-intl";
import { useRouter } from "next/navigation";

const Page = () => {
  const router = useRouter();
  return (
    <div className={s.mainContainer}>
      <Image src={sklad} alt={"sklad"} className={s.skladImage} />
      <div>
        <FormattedMessage id={"ul_menu_option_4"}>
          {(value) => {
            return <p className={s.brandsTitle}>{value}</p>;
          }}
        </FormattedMessage>
        <FormattedMessage
          id={"warehouse_logistic_description"}
          values={{
            a: (chunks) => {
              return (
                <a
                  className={s.link}
                  onClick={() => router.push("/transport-logistic")}
                >
                  {chunks}
                </a>
              );
            },
          }}
        >
          {(value) => {
            return <p className={s.text}>{value}</p>;
          }}
        </FormattedMessage>
      </div>
    </div>
  );
};

export default Page;
