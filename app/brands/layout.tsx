"use client";
import React, { useEffect, useState } from "react";

import s from "./brandsLayout.module.scss";
import { FormattedMessage } from "react-intl";

const Layout = ({ children }: { children: React.ReactNode }) => {
  const [topPosition, setTopPosition] = useState<number>(120);
  useEffect(() => {
    if (document) {
      const scrollListener = () => {
        setTopPosition(() => {
          return document.documentElement.scrollTop <= 120
            ? 120 - document.documentElement.scrollTop
            : 0;
        });
      };

      document.addEventListener("scroll", scrollListener);

      return () => document.removeEventListener("scroll", scrollListener);
    }
  }, []);

  return (
    <>
      <header className={s.header} style={{ top: topPosition }}>
        <FormattedMessage id={"ul_own_brends"}>
          {(value) => {
            return <p className={s.headerTitle}>{value}</p>;
          }}
        </FormattedMessage>
        <FormattedMessage id={"full_name"}>
          {(value) => {
            return <p className={s.headerSubTitle}>{value}</p>;
          }}
        </FormattedMessage>
      </header>
      <div style={{ padding: "60px 0px" }}>{children}</div>
    </>
  );
};

export default Layout;
