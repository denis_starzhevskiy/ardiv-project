"use client";
import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import styles from "@/app/brands/brandsLayout.module.scss";
import {
  ImageType,
  popularImages,
} from "@/public/images/most_popular_brands/popularBrands";
import Image, { StaticImageData } from "next/image";
import { useRouter } from "next/navigation";
import {
  AGEEVSKY,
  ageevskymages,
} from "@/public/images/brandProducts/AGEEVSKY/AgeevskyBrand";
import {
  CANNEDMILK,
  cannedMilkNewImages,
} from "@/public/images/brandProducts/CannedMILK/cannedMilkbrand";
import {
  CHOCOLAND,
  chocolandImages,
} from "@/public/images/brandProducts/CHOCOLAND/chocoland";
import {
  IMPIREOFJAM,
  impireOfJamImages,
} from "@/public/images/brandProducts/ImpireOfJAM/ImpireOfJam";
import {
  MERENGA,
  merengaImages,
} from "@/public/images/brandProducts/MERENGA/marenga";
import {
  MUCHSKINO,
  muchskinoImages,
} from "@/public/images/brandProducts/Muchskino/muchskino";
import {
  SNACCHIPS,
  snacchipsImages,
} from "@/public/images/brandProducts/SNACCHIPS/snacchips";
import {
  SWEETSOTHER,
  sweetsOtherImages,
} from "@/public/images/brandProducts/SweetsOther/sweetsOther";
import {
  MORSHINSKAYA,
  morshinskayaImages,
} from "@/public/images/brandProducts/WaterMORSHINSKAYA/morshinskaya";

import {
  YARILO,
  yariloImages,
} from "@/public/images/brandProducts/YARILO/Yarilo";

import {
  LONDONCLUB,
  londonclubImages,
} from "@/public/images/brandProducts/LondonClub/londonClub";
import {
  MAXIMUS,
  maximusImages,
} from "@/public/images/brandProducts/maximuss/maximus";
import {
  GURMINA,
  gurminaImages,
} from "@/public/images/brandProducts/GURMINA/gurmina";
import {
  GOODMORNING,
  goodmorningImages,
} from "@/public/images/brandProducts/Goodmorning/goodmorning";
import {
  WISH,
  wishNewImages,
} from "@/public/images/brandProducts/Wishbrand/wish";
import { useMediaQuery } from "@mui/material";
import {
  KRUTON,
  krutonImages,
} from "@/public/images/brandProducts/KRUTON/kruton";
import {
  ASCANIA,
  ascaniaImages,
} from "@/public/images/brandProducts/ASCANIA/ascania";
import {
  SINGOFCARE,
  singOfCareNewImages,
} from "@/public/images/brandProducts/SINGOFCARE/singOfCare";
import {
  BOCHKARI,
  bochkariImages,
} from "@/public/images/brandProducts/Bochkari/bochkari";
import {
  SCANDIA,
  scandiaImages,
} from "@/public/images/brandProducts/SCANDIA/scandia";
import {
  VORONZOVSKY,
  voronzovskyImages,
} from "@/public/images/brandProducts/Voronzovsky/voronzovsky";
import {
  AMAPOLA,
  amapolaImages,
} from "@/public/images/brandProducts/Amapola/amapola";
import { DONKO, donkoImages } from "@/public/images/brandProducts/DONKO/donko";
import {
  DELICATE,
  delicateNewImages,
} from "@/public/images/brandProducts/DELICATE/delicate";

const brandsBySections = [
  SINGOFCARE,
  WISH,
  CANNEDMILK,
  DELICATE,
  DONKO,
  VORONZOVSKY,
  GURMINA,
];

const isBrandBySelections = (currentBrandName: string): boolean => {
  return brandsBySections.includes(currentBrandName);
};

const getCurrentBrandImages = (
  currentBrandName: string
): ImageType[] | null => {
  switch (currentBrandName) {
    case AGEEVSKY:
      return ageevskymages;
    case CHOCOLAND:
      return chocolandImages;
    case IMPIREOFJAM:
      return impireOfJamImages;
    case MERENGA:
      return merengaImages;
    case MUCHSKINO:
      return muchskinoImages;
    case SNACCHIPS:
      return snacchipsImages;
    case KRUTON:
      return krutonImages;
    case SWEETSOTHER:
      return sweetsOtherImages;
    case MORSHINSKAYA:
      return morshinskayaImages;
    case YARILO:
      return yariloImages;
    case LONDONCLUB:
      return londonclubImages;
    case MAXIMUS:
      return maximusImages;
    case GOODMORNING:
      return goodmorningImages;
    case ASCANIA:
      return ascaniaImages;
    case BOCHKARI:
      return bochkariImages;
    case SCANDIA:
      return scandiaImages;
    case AMAPOLA:
      return amapolaImages;
    default:
      return null;
  }
};

const getCurrentBrandImagesBySections = (
  currentBrandName: string
): { [key: string]: ImageType[] } => {
  switch (currentBrandName) {
    case SINGOFCARE:
      return singOfCareNewImages;
    case WISH:
      return wishNewImages;
    case CANNEDMILK:
      return cannedMilkNewImages;
    case DELICATE:
      return delicateNewImages;
    case DONKO:
      return donkoImages;
    case VORONZOVSKY:
      return voronzovskyImages;
    case GURMINA:
      return gurminaImages;
    default:
      return {};
  }
};

const getCurrentBrandName = (currentBrandName: string | undefined) => {
  switch (currentBrandName) {
    case AGEEVSKY:
      return "Агеевский";
    case CANNEDMILK:
      return "Сгущенное молоко";
    case CHOCOLAND:
      return CHOCOLAND;
    case IMPIREOFJAM:
      return "Империя джемов";
    case MERENGA:
      return "Меренга";
    case MUCHSKINO:
      return "Мишкино счастье";
    case SNACCHIPS:
      return "Чипсы";
    case KRUTON:
      return "Гренки";
    case SWEETSOTHER:
      return "Родные лакомства";
    case MORSHINSKAYA:
      return "Моршинская";
    case WISH:
      return "Wish";
    case YARILO:
      return "Ярило";
    case LONDONCLUB:
      return "London club";
    case GOODMORNING:
      return "Доброе утро";
    case MAXIMUS:
      return "Maximus";
    case GURMINA:
      return "Gurmina";
    case ASCANIA:
      return "Acania";
    case SINGOFCARE:
      return "Знак заботы";
    case BOCHKARI:
      return "Бочкари";
    case SCANDIA:
      return "Scandia";
    case VORONZOVSKY:
      return "Воронцовские";
    case AMAPOLA:
      return "Amapola";
    case DONKO:
      return "Donko";
    case DELICATE:
      return "Delicate";
    default:
      return [];
  }
};

const Page = () => {
  const router = useRouter();
  const [currentBrandName, setCurrentBrandName] = useState<
    string | undefined
  >();
  const mdUp = useMediaQuery("(min-width:900px)");

  function handleClick(brandName: string | undefined) {
    setCurrentBrandName(brandName);
  }

  function getImageComponent(brand: ImageType, idx: number) {
    return (
      <div
        key={idx}
        style={
          brand.isNoImage
            ? {
                border: "1px solid black",
                borderRadius: "10px",
                height: "400px",
              }
            : { backgroundColor: "white" }
        }
        className={styles.imageContainer}
        onClick={() => handleClick(brand?.name)}
      >
        {!brand.isNoImage && (
          <Image key={idx} src={brand.image} alt={""} width={350} />
        )}
        {!brand.isNoImage || currentBrandName ? (
          ""
        ) : (
          <p style={{ color: "black" }}>{getCurrentBrandName(brand.name)}</p>
        )}
      </div>
    );
  }

  console.log(currentBrandName);

  return (
    <main
      style={{
        padding: "30px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      {mdUp ? (
        <div className={styles.buttonContainer}>
          {currentBrandName && (
            <button
              onClick={() => handleClick(undefined)}
              className={styles.button1}
            >
              <FormattedMessage id={"return_to_brands"}>
                {(value) => {
                  return <p className={styles.brandsTitle}> ⭠ {value} </p>;
                }}
              </FormattedMessage>
            </button>
          )}
          {currentBrandName ? (
            <p className={styles.brandsTitle}>
              {getCurrentBrandName(currentBrandName)}
            </p>
          ) : (
            <FormattedMessage id={"most_popular_brands"}>
              {(value) => {
                return <p className={styles.brandsTitle}>{value}</p>;
              }}
            </FormattedMessage>
          )}
        </div>
      ) : (
        <div className={styles.buttonContainer}>
          {currentBrandName ? (
            <p className={styles.brandsTitle}>
              {getCurrentBrandName(currentBrandName)}
            </p>
          ) : (
            <FormattedMessage id={"most_popular_brands"}>
              {(value) => {
                return <p className={styles.brandsTitle}>{value}</p>;
              }}
            </FormattedMessage>
          )}
          {currentBrandName && (
            <button
              onClick={() => handleClick(undefined)}
              className={styles.button1}
            >
              <FormattedMessage id={"return_to_brands"}>
                {(value) => {
                  return <p className={styles.brandsTitle}> ⭠ {value} </p>;
                }}
              </FormattedMessage>
            </button>
          )}
        </div>
      )}

      <div className={styles.brandsImagesContainer}>
        {!currentBrandName &&
          popularImages.map((image, key) => getImageComponent(image, key))}
      </div>
      <div>
        {!!currentBrandName &&
          (!isBrandBySelections(currentBrandName) ? (
            <div className={styles.brandsImagesContainer}>
              {getCurrentBrandImages(currentBrandName)?.map((image, key) =>
                getImageComponent(image, key)
              )}
            </div>
          ) : (
            <>
              {Object.keys(
                getCurrentBrandImagesBySections(currentBrandName)
              ).map((key) => {
                return (
                  <div key={key}>
                    <p
                      style={{
                        fontSize: "25px",
                        fontWeight: "600",
                        textAlign: "center",
                      }}
                    >
                      {key}
                    </p>
                    <div className={styles.brandsImagesContainer}>
                      {getCurrentBrandImagesBySections(currentBrandName)[
                        key
                      ].map((brand, idx) => {
                        return (
                          <div
                            key={idx}
                            style={
                              brand.isNoImage
                                ? {
                                    border: "1px solid black",
                                    borderRadius: "10px",
                                    height: "400px",
                                  }
                                : { backgroundColor: "white" }
                            }
                            className={styles.imageContainer}
                            onClick={() => handleClick(brand?.name)}
                          >
                            {!brand.isNoImage && (
                              <Image
                                key={idx}
                                src={brand.image}
                                alt={""}
                                width={350}
                              />
                            )}
                            {!brand.isNoImage || currentBrandName ? (
                              ""
                            ) : (
                              <p style={{ color: "black" }}>
                                {getCurrentBrandName(brand.name)}
                              </p>
                            )}
                          </div>
                        );
                      })}
                    </div>
                  </div>
                );
              })}
            </>
          ))}
      </div>
    </main>
  );
};

export default Page;
