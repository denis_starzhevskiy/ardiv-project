import { object, string } from "yup";
import { Controller, SubmitHandler, useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import s from "./styles.module.scss";
import { FormattedMessage, useIntl } from "react-intl";
import Modal from "react-modal";
import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faXmark } from "@fortawesome/free-solid-svg-icons";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import ru from "react-phone-input-2/lang/ru.json";
import { LOCALES } from "@/public/i18/locales";

export type FormValues = {
  firstName: string;
  phone: string;
  email: string;
  message: string;
};

const requiredFieldMessage = "contactsFormRequiredFieldMessage";
const schema = object({
  firstName: string().required(requiredFieldMessage),
  phone: string().matches(/(^$|^[0-9]+$)/, "contactsFormPhoneInvalidMessage"),
  email: string()
    .email("contactsFormEmailInvalidMessage")
    .required(requiredFieldMessage),
  message: string().required(requiredFieldMessage),
});

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};

const ContactForm = () => {
  const intl = useIntl();
  const [successModalOpen, setSuccessModalOpen] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
    control,
  } = useForm<FormValues>({
    defaultValues: {
      firstName: "",
      phone: "",
      email: "",
      message: "",
    },
    resolver: yupResolver(schema),
  });

  const onSubmit: SubmitHandler<FormValues> = async (values) => {
    await fetch("/api/contact-us", {
      method: "POST",
      body: JSON.stringify(values),
    });
    setSuccessModalOpen(true);
    reset();
  };

  return (
    <div>
      <Modal
        isOpen={successModalOpen}
        onRequestClose={() => setSuccessModalOpen(false)}
        style={customStyles}
      >
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "10px",
          }}
        >
          <FontAwesomeIcon
            icon={faXmark}
            onClick={() => setSuccessModalOpen(false)}
          />
        </div>
        <div>
          <FormattedMessage id={"contactsFormSuccessMessage"} />
        </div>
      </Modal>
      <h3 className={s.sectionTitle}>
        <FormattedMessage id={"contactsContactsTitle"} />
      </h3>
      <form
        onSubmit={handleSubmit(onSubmit)}
        className={s.contactFormContainer}
      >
        <div>
          <input
            {...register("firstName")}
            placeholder={intl.formatMessage({
              id: "contactsFormFistNameFieldPlaceHolder",
            })}
            className={s.fullWidth}
          />
          {!!errors.firstName && (
            <p className={s.errorFont}>
              <FormattedMessage id={errors.firstName.message} />
            </p>
          )}
        </div>
        <div>
          <Controller
            name="phone"
            control={control}
            render={({ field }) => (
              <PhoneInput
                key={intl.locale}
                country={"ru"}
                localization={intl.locale === LOCALES.RUSSIAN ? ru : undefined}
                inputStyle={{ width: "100%" }}
                placeholder={intl.formatMessage({
                  id: "contactsFormPhoneFieldPlaceHolder",
                })}
                inputProps={{ name: field.name }}
                value={field.value}
                onChange={(phone) => field.onChange(phone)}
              />
            )}
          />
          {!!errors.phone && (
            <p className={s.errorFont}>
              <FormattedMessage id={errors.phone.message} />
            </p>
          )}
        </div>
        <div>
          <input
            {...register("email")}
            placeholder={intl.formatMessage({
              id: "contactsFormEmailFieldPlaceHolder",
            })}
            className={s.fullWidth}
          />
          {!!errors.email && (
            <p className={s.errorFont}>
              <FormattedMessage id={errors.email.message} />
            </p>
          )}
        </div>
        <div>
          <textarea
            {...register("message")}
            placeholder={intl.formatMessage({
              id: "contactsFormMessageFieldPlaceHolder",
            })}
            rows={7}
            className={s.fullWidth}
          />
          {!!errors.message && (
            <p className={s.errorFont}>
              <FormattedMessage id={errors.message.message} />
            </p>
          )}
        </div>
        <div>
          <button type={"submit"} className={s.sendButton}>
            <FormattedMessage id={"contactsFormSendButton"} />
          </button>
        </div>
      </form>
    </div>
  );
};

export default ContactForm;
