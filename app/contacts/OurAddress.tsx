import s from "@/app/contacts/styles.module.scss";
import Image from "next/image";
import telephoneImage from "@/public/images/telephone.png";
import faxImage from "@/public/images/fax.png";
import React from "react";
import { FormattedMessage } from "react-intl";

export const OurAddress = () => {
  return (
    <div>
      <h3 className={s.underLinedSectionTitle}>
        <FormattedMessage id={"contactsOurAddressTitle"} />
      </h3>
      <h4 className={s.companyName}>ARDIV LTD</h4>
      <p className={s.address}>
        <FormattedMessage id={"contactsAddress"} />
      </p>
      <div className={s.listItem}>
        <Image src={telephoneImage} alt={"telephone"} />
        <p>+972-4-8581792</p>
      </div>
      <div className={s.listItem}>
        <Image src={faxImage} alt={"fax"} />
        <p>+972-4-8581792</p>
      </div>
    </div>
  );
};
