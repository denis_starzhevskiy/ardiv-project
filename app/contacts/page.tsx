"use client";
import React from "react";
import s from "./styles.module.scss";
import ContactForm from "@/app/contacts/ContactForm";
import { OurAddress } from "@/app/contacts/OurAddress";
import Link from "@/components/Link/Link";
import Image from "next/image";
import wazeImage from "@/public/images/waze.jpg";
import { FormattedMessage } from "react-intl";
import spotImage from "@/public/images/spot.jpg";

const Contact = () => {
  return (
    <main className={s.container}>
      <div className={s.max1200Container}>
        <div className={s.gridContainer}>
          <ContactForm />
          <OurAddress />
          <div>
            <Image
              src={spotImage}
              alt={"spot"}
              style={{ objectFit: "cover", width: "100%", height: 300 }}
            />
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m17!1m12!1m3!1d9854.961208055161!2d35.086298252859415!3d32.88408734009074!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m2!1m1!2zMzLCsDUzJzAyLjkiTiAzNcKwMDUnNDEuMCJF!5e1!3m2!1sen!2sua!4v1684315364519!5m2!1sen!2sua"
              width="100%"
              height="300"
              style={{ border: 0, marginBottom: 15 }}
              loading="lazy"
              referrerPolicy="no-referrer-when-downgrade"
            ></iframe>
            <p>
              <Image src={wazeImage} alt={"waze"} className={s.wazeImage} />
              <FormattedMessage id={"contactsNavigateWith"} />-{" "}
              <Link href={"https://waze.com/ul/hsvbgnd6dc"}>Waze</Link>
            </p>
          </div>
        </div>
      </div>
    </main>
  );
};

export default Contact;
