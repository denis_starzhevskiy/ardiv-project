import { LOCALES } from "@/public/i18/locales";
import React, { useEffect, useState } from "react";
import { usePathname, useRouter } from "next/navigation";
import { FormattedMessage } from "react-intl";
import s from "@/app/layout.module.scss";
import Image from "next/image";
import logoImage from "@/public/images/logo.png";
import { DropdownItem } from "@/app/DropdownItem";
import { LanguageOutlined } from "@mui/icons-material";
import { MobileMenu } from "@/components/mobileHeaderNav/mobileNavigation";
import UnitedKingdomFlagImage from "@/public/images/flags/United-Kingdom.png";
import IsraelFlagImage from "@/public/images/flags/Israel.png";

type Props = {
  currentLanguage: LOCALES;
  setLanguage: (newLanguage: LOCALES) => void;
};

export const Header: React.FC<Props> = ({ currentLanguage, setLanguage }) => {
  const [topPosition, setTopPosition] = useState<number>(30);
  const router = useRouter();
  const pathname = usePathname();

  const aboutUsOptions = [
    {
      name: "opt1",
      label: <FormattedMessage id={"ul_menu_option_1"} />,
      onClick: () => router.push("/about-us"),
      checkParam: "/about-us",
    },
    {
      name: "opt2",
      label: <FormattedMessage id={"ul_menu_option_2"} />,
      onClick: () => router.push("/sales-structure"),
      checkParam: "/sales-structure",
    },
    {
      name: "opt3",
      label: <FormattedMessage id={"ul_menu_option_3"} />,
      onClick: () => router.push("/transport-logistic"),
      checkParam: "/transport-logistic",
    },
    {
      name: "opt4",
      label: <FormattedMessage id={"ul_menu_option_4"} />,
      onClick: () => router.push("/warehouse-logistic"),
      checkParam: "/warehouse-logistic",
    },
  ];

  const languageOptions = [
    {
      name: "opt1",
      label: (
        <div className={s.languageDropdownListItem}>
          <Image src={UnitedKingdomFlagImage} alt={"United Kingdom flag"} />
          <div>
            <FormattedMessage id={"english_language"} />
          </div>
        </div>
      ),
      onClick: () => setLanguage(LOCALES.ENGLISH),
      checkParam: "en",
    },
    {
      name: "opt2",
      label: (
        <div className={s.languageDropdownListItem}>
          <div>
            <FormattedMessage id={"russian_language"} />
          </div>
        </div>
      ),
      onClick: () => setLanguage(LOCALES.RUSSIAN),
      checkParam: "ru",
    },
    {
      name: "opt3",
      label: (
        <div className={s.languageDropdownListItem}>
          <Image src={IsraelFlagImage} alt={"Israel flag"} />
          <div>
            <FormattedMessage id={"hebrew_language"} />
          </div>
        </div>
      ),
      onClick: () => setLanguage(LOCALES.HEBREW),
      checkParam: "he",
    },
  ];

  useEffect(() => {
    const scrollListener = () => {
      setTopPosition(() => {
        return document.documentElement.scrollTop <= 30
          ? 30 - document.documentElement.scrollTop
          : 0;
      });
    };

    document.addEventListener("scroll", scrollListener);

    return () => document.removeEventListener("scroll", scrollListener);
  }, []);

  return (
    <>
      <div className={s.preHeader} style={{ top: topPosition - 30 }}>
        <p>+97248581792</p>
        <p>office@ardiv.co.il</p>
      </div>
      <div className={s.header} style={{ top: topPosition }}>
        <Image
          src={logoImage}
          alt={"logo"}
          width={130}
          onClick={() => router.push("/")}
        />
        <ul className={s.navigation}>
          <li
            style={pathname === "/" ? { color: "#f74e37" } : {}}
            onClick={() => router.push("/")}
          >
            ARDIV LTD
          </li>
          <li>
            <DropdownItem
              placement={"bottom"}
              buttonText={<FormattedMessage id="ul_header_about_company" />}
              options={aboutUsOptions}
              value={pathname}
            />
          </li>
          <li
            style={pathname === "/brands" ? { color: "#f74e37" } : {}}
            onClick={() => router.push("/brands")}
          >
            <FormattedMessage id={"ul_own_brends"} />
          </li>
          <li
            style={pathname === "/contacts" ? { color: "#f74e37" } : {}}
            onClick={() => router.push("/contacts")}
          >
            <FormattedMessage id={"ul_contacts"} />
          </li>
          <li className={s.changeLanguageContainer}>
            <DropdownItem
              placement={"bottom-end"}
              buttonText={<FormattedMessage id="ul_select_label" />}
              options={languageOptions}
              value={currentLanguage.toString().split("-")[0]}
              styles={{
                width: "180px",
                padding: "10px",
                textAlign: "center",
              }}
              labelIcon={<LanguageOutlined />}
            />
          </li>
        </ul>
        <div className={s.mobileNavigation}>
          <MobileMenu
            currentLanguage={currentLanguage}
            setLanguage={setLanguage}
          />
        </div>
      </div>
      <div className={s.headerOffset}></div>
    </>
  );
};
