"use client";
import "./globals.css";
import { Inter } from "next/font/google";
import { Locations, messages } from "@/public/i18/messages";
import { IntlProvider } from "react-intl";
import { LOCALES } from "@/public/i18/locales";
import React, { useEffect, useState } from "react";
import { Footer } from "@/app/Footer";
import { Header } from "@/app/Header";

const inter = Inter({ subsets: ["latin"] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const [locale, setLocale] = useState<LOCALES>(LOCALES.ENGLISH);

  useEffect(() => {
    const locale = localStorage.getItem("locale");
    if (locale !== null && Object.values(LOCALES).includes(locale as LOCALES)) {
      setLocale(locale as LOCALES);
    }
  }, []);

  const changeLanguage = (newLanguage: LOCALES) => {
    setLocale(newLanguage);
    localStorage.setItem("locale", newLanguage);
  };

  return (
    <html lang="en">
      <body className={inter.className}>
        <IntlProvider
          locale={locale}
          messages={messages[locale as Locations]}
          defaultLocale={LOCALES.RUSSIAN}
        >
          <Header currentLanguage={locale} setLanguage={changeLanguage} />
          <main>{children}</main>
          <Footer />
        </IntlProvider>
      </body>
    </html>
  );
}
